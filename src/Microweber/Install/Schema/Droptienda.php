<?php

namespace Microweber\Install\Schema;

class Droptienda
{
    public function get()
    {
        return [
            'wishlist_sessions' => [
                'user_id' => 'integer',
                'name' => 'string',
                'updated_at' => 'dateTime',
                'created_at' => 'dateTime',
            ],

            'wishlist_session_products' => [
                'updated_at' => 'dateTime',
                'created_at' => 'dateTime',
                'user_id' => 'integer',
                'wishlist_id' => 'integer',
                'product_id' => 'integer',

            ],

            'dynamic_banners' => [
                'user_id' => 'integer',
                'media_id' => 'integer',
                'coords' => 'string',
                'product_id' => 'integer',
                'updated_at' => 'dateTime',
                'created_at' => 'dateTime',
            ],

            'wishlist_link' => [
                'updated_at' => 'dateTime',
                'created_at' => 'dateTime',
                'user_id' => 'integer',
                'products_id' => 'integer',
                'slug' => 'string',
            ],

            'quick_checkout' => [
                'updated_at' => 'dateTime',
                'created_at' => 'dateTime',
                'user_id' => 'integer',
                'products_id' => 'integer',
                'slug' => 'string',
                'is_deliver' => 'string',

            ],

            'legals' => [
                'updated_at' => 'dateTime',
                'created_at' => 'dateTime',
                'term_name' => 'string',
                'description' => 'string',
                'counter' => 'string',

            ],

            'sync_history' => [
                'sync_type' => 'string',
                'sync_event' => 'string',
                'model_id' => 'integer',
                'synced_at' => 'dateTime',
                'drm_ref_id' => 'integer',
                'tries' => array('type' => 'tinyInteger', 'default' => 0),
                'exception' => 'text',
                'response' => 'text',
                'created_at' => 'dateTime',
                'updated_at' => 'dateTime',
                'deleted_at' => 'dateTime',
            ],

            'tokenurl' => [
                'token' => 'string',
                'url' => 'string',
            ]


        ];
    }
}
