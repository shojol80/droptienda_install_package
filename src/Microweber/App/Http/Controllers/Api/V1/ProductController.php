<?php

namespace Microweber\App\Http\Controllers\Api\V1;

use App\Models\Category;
use App\Models\CategoryItem;
use App\Models\CustomField;
use App\Models\CustomFieldValue;
use App\Models\Media;
use Illuminate\Http\Request;
use Microweber\App\Http\Controllers\Controller;
use Microweber\App\Services\Product\ProductService;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    private $service;

    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = $this->service->all($request->all())->toArray();

        return response()->json($data);
    }

    public function show($id): \Illuminate\Http\JsonResponse
    {
        $data = $this->service->getById($id);

        return response()->json($data);
    }

    public function store(Request $request)
    {
        try {
            $product = $this->service->store(array_merge(
                    $request->only([
                        'title',
                        'drm_ref_id',
                        'description',
                        'ean',
                        'content_type',
                        'subtype',
                        'is_active',
                    ]),
                    ['url' => mw()->url_manager->slug($request->get('title'))]
                )
            );

            $this->updateContentData($request, $product);

            app()->log_manager->save('is_system=y');

            return response()->json($product);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 422);
        }
    }

    public function update($id, Request $request)
    {
        try {
            $data = $this->service->update($id, array_merge(
                    $request->only([
                        'title',
                        'drm_ref_id',
                        'description',
                        'ean',
                        'content_type',
                        'subtype',
                        'parent',
                        'is_active',
                    ]),
                    ['url' => mw()->url_manager->slug($request->get('title'))]
                )
            );

            $this->updateContentData($request, $data);

            app()->log_manager->save('is_system=y');

            return response()->json($data);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 422);
        }
    }

    public function destroy($id): \Illuminate\Http\JsonResponse
    {
        try {
            $this->service->destroy($id);
            app()->log_manager->save('is_system=y');

            return response()->json(['success' => true, 'message' => 'Product deleted successfully']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 422);
        }
    }

    public function updateContentData($request, $product)
    {
        DB::table('media')->where([
            'rel_type' => 'content',
            'media_type' => 'picture',
            'rel_id' => $product->id,
        ])->delete();

        foreach ($request->get('images', []) as $image) {
            DB::table('media')->insert([
                'rel_type' => 'content',
                'media_type' => 'picture',
                'rel_id' => $product->id,
                'filename' => $image
            ]);
        }

        // saving content custom fields - price
        $priceField = CustomField::updateOrCreate(
            [
                'rel_type' => 'content',
                'rel_id' => $product->id,
                'type' => 'price',
            ],
            ['name' => 'price']
        );
        CustomFieldValue::updateOrCreate(['custom_field_id' => $priceField->id], ['value' => $request->get('price', 0)]);

        // saving category
        CategoryItem::where(['rel_type' => 'content', 'rel_id' => $product->id])->delete();
        foreach ($request->get('categories', []) as $catId) {
            $category = Category::where('drm_ref_id', $catId)->first();
            if ($category) {
                DB::table('categories_items')->insert([
                    'parent_id' => $category->id,
                    'rel_type' => 'content',
                    'rel_id' => $product->id,
                ]);
            }
        }

        // saving variants
        $colors = $request->get('colors', []);
        if (!empty($colors)) {
            $colorField = CustomField::updateOrCreate([
                'rel_type' => 'content',
                'rel_id' => $product->id,
                'type' => 'text',
                'name' => 'color',
            ]);

            CustomFieldValue::where('custom_field_id', $colorField->id)->delete();
            foreach($colors as $color) {
                CustomFieldValue::create([
                    'custom_field_id' => $colorField->id,
                    'value' => trim($color)
                ]);
            }
        }

        $sizes = $request->get('sizes', []);
        if (!empty($sizes)) {
            $sizeField = CustomField::updateOrCreate([
                'rel_type' => 'content',
                'rel_id' => $product->id,
                'type' => 'text',
                'name' => 'size',
            ]);

            CustomFieldValue::where('custom_field_id', $sizeField->id)->delete();
            foreach($sizes as $size) {
                CustomFieldValue::create([
                    'custom_field_id' => $sizeField->id,
                    'value' => trim($size)
                ]);
            }
        }
    }

    public function guestCheckout($id)
    {
        $product = \App\Models\Content::where('drm_ref_id', $id)->first();

        if (!$product) {
            return response()->json(['success' => false, 'message' => 'Product not found!'], 422);
        }
        $randomString = uniqid();

        $ins_array = array(
            'products_id' => $product->id,
            'user_id' => 1,
            'slug' => $randomString,
        );

        DB::table('quick_checkout')->insert($ins_array);
        $url = site_url() . 'checkout?slug=' . $randomString;

        return response()->json([
            'success' => true,
            'message' => 'Success',
            'data' => ['url' => $url, 'slug' => $randomString, 'id' => $product->id]
        ]);
    }
}

