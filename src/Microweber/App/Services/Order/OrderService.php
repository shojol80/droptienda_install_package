<?php


namespace Microweber\App\Services\Order;


use App\Enums\SyncEvent;
use App\Models\Category;
use App\Models\Order;
use App\Models\SyncHistory;
use App\Services\DrmSyncService;
use Illuminate\Support\Facades\DB;
use Microweber\App\Services\BaseService;
use Illuminate\Support\Arr;

class OrderService extends BaseService
{
    public function all(array $filters = [])
    {
        $query = Order::whereHas('carts')->with('carts');

        $limit = Arr::get($filters, 'limit', 20);

        return $limit != '-1' ? $query->paginate($limit) : $query->get();
    }

    public function getById($id)
    {
        return Order::with('carts')->find($id);
    }

    public function store(array $data)
    {
        return $this->saveOrder($data);
    }

    public function update($id, array $data)
    {
        return $this->saveOrder($data, $id);
    }

    public function destroy($id)
    {
        return Order::where('drm_ref_id', $id)->delete();
    }

    private function saveOrder($data, $id = null)
    {
        $product = Order::firstOrNew(['drm_ref_id' => $id]);
        $product->fill($data);
        $product->save();

        return $product;
    }

    public function syncOrderToDrm(SyncHistory $syncHistory)
    {
        $syncService = new DrmSyncService(env('DRM_BACKGROUND_SERVER_URL'));
        switch ($syncHistory->sync_event) {
            case SyncEvent::CREATE:
                try {
                    $order = Order::with('carts')->find($syncHistory->model_id)->toArray();
                    if ($order) {
                        $response = $syncService->storeOrder($order);
                        if (!empty($response['id'])) {
                            DB::table('cart_orders')->update([
                                'drm_ref_id' => $response['id']
                            ]);
                            $syncHistory->update([
                                'response' => json_encode($response),
                                'synced_at' => date('Y-m-d H:i:s'),
                            ]);
                        }
                    }
                    $syncHistory->increment('tries');
                } catch (\Exception $e) {
                    $syncHistory->update([
                        'exception'=>json_encode($e->getMessage()),
                        'tries' => $syncHistory->tries + 1
                    ]);
                }
                break;

            case SyncEvent::UPDATE:
                try {
                    $order = Order::with('carts')->find($syncHistory->model_id)->toArray();
                    if ($order && $order->drm_ref_id) {
                        $response = $syncService->updateOrder($order->drm_ref_id, $order);
                        if (!empty($response['id'])) {
                            DB::table('cart_orders')->update([
                                'drm_ref_id' => $response['id']
                            ]);
                            $syncHistory->update([
                                'response' => json_encode($response),
                                'synced_at' => date('Y-m-d H:i:s'),
                            ]);
                        }
                    }
                    $syncHistory->increment('tries');
                } catch (\Exception $e) {
                    $syncHistory->update([
                        'exception'=>json_encode($e->getMessage()),
                        'tries' => $syncHistory->tries + 1
                    ]);
                }
                break;
        }
    }
}
