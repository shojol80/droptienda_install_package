<?php

use App\Jobs\DrmSyncJob;
use App\Services\DrmSyncService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Microweber\App\Services\Category\CategoryService;
use Illuminate\Support\Facades\DB;
use App\Models\Content;
use Illuminate\Support\Arr;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('products', 'ProductController');
Route::apiResource('categories', 'CategoryController');
Route::apiResource('orders', 'OrderController');
Route::apiResource('customers', 'CustomerController');

// customer
Route::get('drm-guest-checkout/{id}', 'ProductController@guestCheckout');

// category sync
Route::get('sync/categories', function() {
	$syncHistory = \App\Models\SyncHistory::find(11);
	dd( app(CategoryService::class)->syncCategoryToDrm($syncHistory) );
});

// customer
Route::get('sync/customers/{id}', function ($id) {
	return app('\App\Services\DrmSyncService')->deleteCustomer($id);
});

Route::get('product/{code}', function ($code){
    $product = \App\Models\Content::where('drm_ref_id', $code)->first();

    if(!$product) {
        return response()->json(['success' => false, 'message' => 'Product not found!'], 422);
    }
    $randomString = uniqid();

    $ins_array = array(
        'products_id' => $product->id,
        'user_id' => 1,
        'slug' => $randomString,
    );

    DB::table('quick_checkout')->insert($ins_array);
    $url = site_url() . 'checkout?slug=' . $randomString;

    return redirect('checkout?slug=' . $randomString);
});

Route::get('validate-droptienda-shop', function (Request $request) {
    $userToken = $request->input('userToken');
    $userPassToken = $request->input('userPassToken');

    if(!empty($userToken) && $userToken == config('microweber.userToken') && !empty($userPassToken) && $userPassToken == config('microweber.userPassToken')) {
        return response()->json(['success' => false, 'message' => 'Droptienda Shop verified successfully.'], 200);
    }

    return response()->json(['success' => false, 'message' => 'Droptienda Shop verification failed.'], 401);
});
