<?php
// var_dump($data);var_dump($settings);die();
?>
<div class="col-md-<?php echo $settings['field_size']; ?>">
    <div class="mw-ui-field-holder">

        <?php if($settings['show_label']): ?>
        <label class="control-label">
            Adresse
            <?php if ($settings['required']): ?>
            <span style="color: red;">*</span>
            <?php endif; ?>
        </label>
        <?php endif; ?>





    <div class="module module-custom-fields" data-mw-title="Custom fields"
        id="shipping-info-address-cart-checkout-shop-shipping-shop-shipping-gateways-country" data-for="module"
        default-fields="address" input-class="field-full form-control" data-skip-fields="country"
        data-type="custom_fields" parent-module-id="cart-checkout-shop-shipping-shop-shipping-gateways-country"
        parent-module="shop/shipping/gateways/country"><input type="hidden" name="for_id"
            value="cart-checkout-shop-shipping-shop-shipping-gateways-country"
            wtx-context="5F068772-F445-4234-9818-6BE25BB5B206">
        <input type="hidden" name="for" value="module" wtx-context="0C013BC6-10AC-42B3-8E2E-1F4FD0C4C573">
        <div class="row">


            <div class="col-md-12" contenteditable="false">
                <div class="mw-ui-field-holder">
                    <div class="control-group" style="margin-bottom: 10px;">

                        <label class="control-label">Postleitzahl <span style="color: red;">*</span></label>

                        <input type="text" class="form-control input-required" name="Address[zip]" data-custom-field-id="1"
                            wtx-context="3527ADEA-82DC-4388-9789-A1445B3857E1" required>

                    </div>
                    <div class="control-group" style="margin-bottom: 10px;">

                        <label class="control-label">Stadt <span style="color: red;">*</span></label>

                        <input type="text" class="form-control input-required" name="Address[city]" data-custom-field-id="1"
                            wtx-context="EEAE35A0-7872-4188-B888-331AEAC9A9E3" required>

                    </div>



                    <div class="control-group" style="margin-bottom: 10px;">

                        <label class="control-label ">Bundesland </label>

                        <input type="text" class="form-control" name="Address[state]" data-custom-field-id="1"
                            wtx-context="268FD460-6B51-4516-8B21-70B126A7F8BC">

                    </div>

                    <div class="control-group" style="margin-bottom: 10px;">

                        <label class="control-label">Straße <span style="color: red;">*</span></label>

                        <input type="text" class="form-control input-required" name="Address[address]" data-custom-field-id="1"
                            wtx-context="98B0C40D-FCD4-4DB9-8271-5FF62FFC9A07" required>

                    </div>



                </div>
            </div>


        </div>
    </div>


    <?php if ($data['help']): ?>
    <span class="help-block"><?php echo $data['help']; ?></span>
    <?php endif; ?>
</div>
</div>
