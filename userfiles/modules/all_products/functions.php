<?php
 event_bind('content.after.save', function() {
 	    //setup the request, you can also use CURLOPT_URL
 	$ch4 = curl_init('https://droptienda.rocks/api/all_products');
 	// Returns the data/output as a string instead of raw data
 	curl_setopt($ch4, CURLOPT_RETURNTRANSFER, true);
 	curl_setopt($ch4, CURLOPT_CUSTOMREQUEST, "GET");
	// get stringified data/output. See CURLOPT_RETURNTRANSFER
 	$data = curl_exec($ch4);
 	// get info about the request
 	$info = curl_getinfo($ch4);
 	// close curl resource to free up system resources
 	curl_close($ch4);
 });

api_expose('all_products');
function all_products($params=false){
	$all_products = DB::table('content')->where('content_type','product')
	->join('media', 'media.rel_id', '=', 'content.id')
	->join('tagging_tagged', 'tagging_tagged.taggable_id', '=', 'content.id')
	->join('content_data', 'content.id', '=', 'content_data.content_id')
	->join('custom_fields', 'custom_fields.rel_id', '=', 'content.id')
	->join('custom_fields_values', 'custom_fields_values.custom_field_id', '=', 'custom_fields.id')
	->where('content.is_deleted',0)
	->select('content.id as content_id','content.*','media.filename','custom_fields_values.value','content_data.*','tagging_tagged.tag_name')
	->groupBy('content.id');
	if(isset($_GET['pid'])) {
		$all_products=$all_products->where('content_id',$_GET['pid'])->get();
	}else {
		$all_products=$all_products->get();
	}
	$all_products = str_replace('{SITE_URL}','https://droptienda.rocks/',$all_products);
	//dd($all_products);
	$all_products = json_decode($all_products);
	DB::beginTransaction();
	try {
		$client = new \GuzzleHttp\Client();
		foreach ($all_products as $product) {
			
			$categories = DB::table('categories_items')
						->join('categories', 'categories.id', '=', 'categories_items.parent_id')
						->where('categories_items.rel_id',$product->content_id)
						->select('categories.title as category')
					    ->pluck('category')->toArray();
			try{
				$response = $client->request('PUT', 'http://165.227.134.199/api/v1/catalogs/products',[
					'headers'	=> [
						'content-type' => 'application/json',
						'Authorization' => 'Bearer ' . config('global.token_dp'),
						
					],
					'query' => [
				        'title' => $product->title,
						'item_number' => 'null',
						'ean' => $product->ean,
						'description' => $product->content_body??"No content",
						'image' => [$product->filename],
						'ek_price' => (float)$product->value,
						'vk_price' => (float)$product->value,
						'stock' => (int)$product->field_value,
						'gender' =>'Male',
						'categories' => $categories,
						'status' => '1000',
						'suplier' => 1133,
						'country' => '1',
						'item_weight' => 23,
						'item_color' => 'null',
						'production_year' => 'null',
						'materials' => 'null',
						'brand' => 'null',
						'tags' => $product->tag_name,
						'item_size' => 'null',
						'delivery_days' => 5,
				    ],
			    
				]);
			}catch(\Exception $e){
				dump($e->getMessage());
			}
		}
		
	    DB::commit(); // all good
	}
	catch (\Exception $e) {
	    DB::rollback(); // something went wrong
	    $e->getMessage();
	}
}

api_expose('save_product');
function save_product($params=false){

	DB::beginTransaction();
	try {

		$client = new \GuzzleHttp\Client();
		
		$response = $client->request('GET', 'http://165.227.134.199/api/v1/catalogs/products',[
			'headers'	=> [
				'content-type' => 'application/json',
				'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiYmMyMTc2NzlkZjZhOTdhMzU3ZjQwMzdlMjNmMjM3YTYyY2I1M2FiNGJjYmQwNDNiYTNhOTEwYzI2OTU2NzYzMTBiZWE0ZDY2NDg4NjQyYzAiLCJpYXQiOjE2MDM1MjI2NDcsIm5iZiI6MTYwMzUyMjY0NywiZXhwIjoxNjM1MDU4NjQ3LCJzdWIiOiI2MiIsInNjb3BlcyI6W119.Ky6P1ldQPb6Hlv_GucNZw5rEkk40-2jqctI9i3IuraGpoTPXtHVQl-0fvkItzd8gW6hNhM-9cO-mxLGyFWxAoaiopVAqxqrXc_f6Dfz2lbGXpYYFlRgNEQZrWRRHMeZr6kZgPszf8IWCcvcV6J7MGjlB-EOQbJ922ATnJdsizRnA0lOpt4q_VDRoGpIER7nRjs68Kbss2RHQ-zoHUk24S1mSR-ulRBFv8eASWvXjyGWp0i4Pp5I0ATRB5cRltH_GuZ690_QNfMZfaGi0KQZRSNTp_T0GuNdCtagYabljSQKZPWWNHesOdd6bUIA7rz_FGWRC0lIKRSSYOGC2oPWVQ6ESn_IbabFTTmUptJBaRCtcQdQzhf3Fv4eIUewB8DIrWvy0oC_T_s-oPJI0G8EThDdkrf-tKvaMna3LWArxiv9DOCmS5Df8leF1HwgJ9U07cfdjCWDwa8d11UANxxdUVrwYdSZjjqPkY3fjInZ-151fY90DssVrWpUMrG5QSuFgCH3ZeqzZtGk8k0G8wtSboVNzJMUa5__9dxWnMIEXKe8LdW2PZxS0P_MVqpuXlgw-CYooZLk402G_zV0loQaolax7ZWdXSGwjUsPMLv-i4X875oeb-txcV8_DdFvL7JWjwcsQWfRTPzRhSlne7N-9OAYc91-cPO8oZaQ-2GVVVpI',
				
			],
			
			'query' => [
				'country' => 1,
			]
		]);

		if($response->getStatusCode() == 200){
			$drmProducts = json_decode($response->getBody()->getContents());
			// dd($drmProducts);
			if($drmProducts){
				foreach ($drmProducts as $drmProduct) {
					// dd($drmProduct);
					$id = DB::table('content')->insertGetId([
					    'ean' => $drmProduct->ean,
					    'title' =>$drmProduct->title,
					    'description' =>$drmProduct->description,
					]);


					DB::table('custom_fields_values')->insert([
					    'value' => $id,
					    'value' => $drmProduct->vk_price,
					]);

					// $name = $_REQUEST['name'];
				}

			}
		}

	    DB::commit(); // all good
	}
	catch (\Exception $e) {
	    DB::rollback(); // something went wrong


	    dd($e->getMessage());
	}
	
}



api_expose('product_delete');
function product_delete($data){
	$delete_data = DB::table('content');
	//dd($_POST);
	//dd($_DELETE);
	if (isset($_POST['product_id'])) {
		$delete_data = $delete_data->where('id',$_POST['product_id'])->delete();
		//$delete_data = delete_category($_DELETE['cid']);
	} else {
		$delete_data = 'product_id ie required';
		return json_encode($delete_data);
	}
	// $category_id = 5;
	// $delete = delete_category($category_id);
}

// api_expose('products');
// function products($params = false)
// {
//     $content = get_products();
//     $all_products = array();
//     $i=0;
//         foreach ($content as $item) {
//             $all_products[$i]['title'] = $item['title'];
//             $all_products[$i]['id'] = $item['id'];
//             $all_products[$i]['url'] = $item['url'];
//             $all_products[$i]['description'] = $item['description'];
//             $all_products[$i]['created_at'] = $item['created_at'];
//             // $_GET['id'] = $item['id'];
//             // $all_products = $item['url'];
//             // $all_products = $item['description'];
//             // $all_products = $item['created_at'];
//             $i++;
//         }
//         // $all_arr = implode('/ ', $all_products);
//     return (json_encode($all_products));

// }


