<?php

$config = array();
$config['name'] = "Empty Element";
$config['author'] = "Droptienda";
$config['description'] = "Droptienda";

$config['help'] = "http://microweber.info/modules/title";
$config['version'] = 0.2;
$config['ui'] = true;
$config['position'] = 5;
$config['as_element'] = 1;
$config['categories'] = "content";
