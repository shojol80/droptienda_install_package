<?php

/*

type: layout

name: Default

description: Default

*/


?>
<?php

if (isset($params['content-id'])) {
    $product = get_content_by_id($params["content-id"]);
    $title = $product['title'];
} else {
    $title = _e("Product", true);
}


?>
<style>
.item-price{
    text-align: center;
    margin-bottom: 8px;
}
.item-cart{
    display:flex;
    align-items:center;
    justify-content:space-between;
}
</style>

<br class="mw-add-to-cart-spacer"/>
        <div class="product-des">
            <p>
            <?php if(get_content_by_id($for_id)['description'] != null) { echo get_content_by_id($for_id)['description']; } ?></p>
        </div>
<module type="custom_fields" data-content-id="<?php print intval($for_id); ?>" data-skip-type="price" id="cart_fields_<?php print $params['id'] ?>"/>
<?php if (is_array($data)): ?>
    <div class="price">


        <?php $i = 1;

        foreach ($data as $key => $v): ?>

            <div class="mw-price-item">

                <div class="item-price">
                <?php

                    $keyslug_class = str_slug(strtolower($key));


                    // $key = $price_offers[$key]['offer_price'];

                    ?>

                    <?php if (is_string($key) and trim(strtolower($key)) == 'price'): ?>

                        <span class="mw-price-item-key mw-price-item-key-<?php print ($keyslug_class); ?>" style="font-size: 16px;font-weight:600;text-transform:capitalize;">
                        Preis
                    </span>
                    <?php else: ?>
                        <span class="mw-price-item-key mw-price-item-key-<?php print ($keyslug_class); ?>" style="font-size: 16px;font-weight:600;text-transform:capitalize;">
                        <?php print $key; ?>
                    </span>
                    <?php endif; ?>:


                    <span class="mw-price-item-value"><?php print currency_format($v); ?></span>
                </div>
                <div class="product-tax-text" style="display:flex;align-items:center;justify-content:center;margin-bottom:10px;font-size: 12px;">
                    <span class="edit" field="content_body">
                    <?php $tax= mw()->tax_manager->get();
                        ?>
                    inkl. <?=intval(!empty($tax['0']['amount']) ? $tax['0']['amount'] : 0)?>% MwSt. zzgl.
                    </span>
                    <span>
                        <a href="<?php print site_url("delivery-conditions"); ?>" style="color:#23a1d1;margin-left:5px;">Versand</a>
                    </span>
                </div>

                <div class="item-cart">
                        <?php if (is_logged()) { ?>

                                <div class="custom-select">
                                    <span class="wishlist-logo" >
                                    <img id="wishlist-heart-<?= $for_id; ?>" src="<?php print template_url(); ?>image/heart-blue.png" alt="">
                                    </span>
                                    <label for="wishlist-select-<?= $for_id; ?>"></label>

                                    <select id="wishlist-select-<?= $for_id; ?>" class="js-example-basic-multiple"
                                            name="states[]" multiple="multiple">
                                    </select>
                                </div>

                        <?php } ?>

                        <div class="item-cart-button">
                            <?php if (!isset($in_stock) or $in_stock == false) : ?>
                                <button class="btn btn-default pull-right" type="button" disabled="disabled"
                                        onclick="Alert('<?php print addslashes(_e("This item is out of stock and cannot be ordered", true)); ?>');">
                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                    <?php _e("Out of stock"); ?>
                                </button>
                            <?php else: ?>


                            <button class="btn btn-default pull-right" type="button"
                                    onclick="mw.cart.add('.mw-add-to-cart-<?php print $params['id'] ?>','<?php print $v ?>', '<?php print $title; ?>');">
                                    <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                <?php _e($button_text !== false ? $button_text : "in den Warenkorb"); ?>
                            </button>
                        </div>

                </div>





                    <?php $i++; endif; ?>


            </div>
            <?php if ($i > 1) : ?>
                <br/>
            <?php endif; ?>
            <?php $i++; endforeach; ?>
    </div>
<?php endif; ?>

<script type="text/javascript">
    <?php if (is_logged()) { ?>
    $(document).ready(() => {

        $('.js-example-basic-multiple').select2();
        var a = $(".select2-selection--single").parent().hide();
        a.parent().hide();

        $.get(`<?= api_url('get_wishlist_sessions'); ?>`, result => {

            const selected = [];
            const list = [];
            result.forEach(function (session) {
                // console.log(session);
                list.push('<option value=' + session['id'] + '>' + session['name'] + '</option>');

                // $("#wishlist-list").append('<li style="display: block;" title="' + session['name'] + '"><a href="shop?wishlist_id=' + session["id"] + '" data-category-id="' + session['id'] + '" title="' + session['name'] + '" class="depth-0">' + session['name'] + '</a></li>');
                session['products'].forEach(function (prod) {
                    if (selected[parseInt(prod['product_id'])] === undefined) {
                        selected[parseInt(prod['product_id'])] = [];
                    }
                    selected[parseInt(prod['product_id'])].push(session.id.toString())
                })
            });

            <?php if (!empty($data)): ?>
            <?php foreach ($data as $key => $v): ?>
            var wishlistProduct = $("#wishlist-select-<?php echo $for_id;?>");
            wishlistProduct.empty();
            wishlistProduct.append('<option disabled value="null"></option>');
            list.forEach(function (value) {

                wishlistProduct.append(value);

            });
            var didd = <?php echo $for_id;?>;
            wishlist_details(didd);



            <?php endforeach; ?>
            <?php endif; ?>

            // console.log(result);
            // console.log("selected length => " ,selected.length);
            // console.log(selected);
            // selected products trigger added
            selected.forEach(function (value, index) {
                const wishlistProduct2 = $("#wishlist-select-" + index.toString());
                // console.log("#wishlist-select-" + index.toString())
                wishlistProduct2.select2().val(value).trigger("change");
            });
            function wishlist_details(didd) {
                // console.log(didd)
                if (selected[didd] && selected[didd].length > 0)
                    $("#wishlist-heart-"+didd).attr("src","<?php print template_url(); ?>image/icon.png");
                else
                    $("#wishlist-heart-"+didd).attr("src","<?php print template_url(); ?>image/heart-blue.png");
            }

        });
    });


    <?php if (!empty($data)): ?>
    <?php foreach ($data as $key => $v): ?>
        $( document ).ready(function(e) {
        // console.log(e);

        <?php if(content_data($for_id)['qty'] == '0'){ ?>

        // console.log('htfyfft');


        removeProduct(<?php echo $for_id;?>, 'all')
        if ($("#wishlist-select-<?php echo $for_id;?>").val().length == 0) {
            $("#wishlist-heart-<?php echo $for_id;?>").attr("src","<?php print template_url(); ?>image/heart-blue.png");

        }

<?php }else{ ?>
    $("#wishlist-select-<?php echo $for_id;?>").on('select2:unselect', function (e) {
        // console.log(e.params.data.id);


        removeProduct(<?php echo $for_id;?>, e.params.data.id)
        if ($("#wishlist-select-<?php echo $for_id;?>").val().length == 0) {
            $("#wishlist-heart-<?php echo $for_id;?>").attr("src","<?php print template_url(); ?>image/heart-blue.png");

        }

    });
<?php } ?>

});

    $("#wishlist-select-<?php echo $for_id;?>").on('select2:select', function (e) {
        addProduct(<?php echo $for_id;?>, e.params.data.id)
        $("#wishlist-heart-<?php echo $for_id;?>").attr("src","<?php print template_url(); ?>image/icon.png");
    });



    <?php endforeach; ?>
    <?php endif; ?>


    function removeProduct(productId, sessionId) {
        $.post("<?php print api_url('remove_wishlist_sessions'); ?>", {
            productId: productId,
            sessionId: sessionId
        }, () => {
        });
    }

    function addProduct(productId, sessionId) {
        $.post("<?php print api_url('add_wishlist_sessions'); ?>", {productId: productId, sessionId: sessionId}, () => {
        });
    }
    <?php } ?>
    function wishlist_filter(wId) {
        $.post("<?php print site_url('shop'); ?>", {wishlist_id: wId}, () => {
        });
    }


</script>
