<style>
    .well{
        min-height:auto !important;
    }
</style>

<div class="mw-ui-row shipping-and-payment mw-shop-checkout-personal-info-holder">
    <div class="mw-ui-col" style="width: 33%;">
        <div class="mw-ui-col-container">
            <?php if(is_logged()){ ?>
            <div class="well checkout-box-shadow-style" style="margin-bottom:30px">
                <?php $user = get_user(); ?>
                <h2 style="margin-top:0 " class="edit nodrop checkout-form-heading" rel_id="<?php print $params['id'] ?>">
                    <?php _e("persönliche Informationen"); ?>
                </h2>
                <hr/>
                <div class="form-group">
                    <label>
                        <?php _e("Vorname"); ?> <span style="color: red;">*</span>
                    </label>
                    <input name="first_name" class="field-full form-control input-required" type="text"
                        value="<?php if (isset($user['first_name'])) {
                            print $user['first_name'];
                        } ?>" required/>
                </div>
                <div class="form-group">
                    <label>
                        <?php _e("Nachname"); ?> <span style="color: red;">*</span>
                    </label>
                    <input name="last_name" class="field-full form-control input-required" type="text"
                        value="<?php if (isset($user['last_name'])) {
                            print $user['last_name'];
                        } ?>" required/>
                </div>
                <div class="form-group">
                    <label>
                        <?php _e("Email"); ?> <span style="color: red;">*</span>
                    </label>
                    <input name="email" class="field-full form-control input-required" type="text"
                        value="<?php if (isset($user['email'])) {
                            print $user['email'];
                        } ?>" required/>
                </div>
                <div class="form-group">
                    <label>
                        <?php _e("Telefon"); ?>
                    </label>
                    <input name="phone" class="field-full form-control" type="text"
                        value="<?php if (isset($user['phone'])) {
                            print $user['phone'];
                        } ?>"/>
                </div>
            </div>
            <?php }else{ ?>

            <div class="well checkout-box-shadow-style" style="margin-bottom:30px; display:none;" id="guest_check">
                <?php $user = get_user(); ?>
                <h2 style="margin-top:0 " class="edit nodrop checkout-form-heading" rel_id="<?php print $params['id'] ?>">
                    <?php _e("persönliche Informationen"); ?>
                </h2>
                <hr/>
                <div class="form-group">
                    <label>
                        <?php _e("Vorname"); ?> <span style="color: red;">*</span>
                    </label>
                    <input name="first_name" class="field-full form-control input-required" type="text"
                        value="<?php if (isset($user['first_name'])) {
                            print $user['first_name'];
                        } ?>" required/>
                </div>
                <div class="form-group">
                    <label>
                        <?php _e("Nachname"); ?> <span style="color: red;">*</span>
                    </label>
                    <input name="last_name" class="field-full form-control input-required" type="text"
                        value="<?php if (isset($user['last_name'])) {
                            print $user['last_name'];
                        } ?>" required/>
                </div>
                <div class="form-group">
                    <label>
                        <?php _e("Email"); ?> <span style="color: red;">*</span>
                    </label>
                    <input name="email" class="field-full form-control input-required" type="text"
                        value="<?php if (isset($user['email'])) {
                            print $user['email'];
                        } ?>" required/>
                </div>
                <div class="form-group">
                    <label>
                        <?php _e("Telefon"); ?>
                    </label>
                    <input name="phone" class="field-full form-control" type="text"
                        value="<?php if (isset($user['phone'])) {
                            print $user['phone'];
                        } ?>"/>
                </div>
            </div>

            <div class="well checkout-box-shadow-style" style="margin-bottom:30px;" id="hideit">
            <p>Für die Kasse als Gast klicken Sie bitte auf die Schaltfläche</p>
            <button type="button" class="btn btn-info" onclick="guest_check()">weiter als Gast</button>

            <hr>


            <p>oder Melden Sie sich mit Ihren Daten an</p>
            <a href="#" class="js-login-modal btn btn-login" data-toggle="modal" data-target="#loginModal">Anmeldung</a>

            <!-- <div id="guest-login">
                <module type="users/login"/>
            </div> -->
            </div>

                 <?php } ?>
            <?php if ($cart_show_shipping != 'n'): ?>
            <div class="well mw-shop-checkout-shipping-holder checkout-box-shadow-style">

                    <module type="shop/shipping"/>

            </div>
            <?php endif; ?>
        </div>

    </div>


</div>


<script>
    function guest_check(){
        $('#hideit').css('display','none');
        $('#guest_check').css('display','block');
    }

    // $("#guest-login").hide();
    // $(".btn-login").on('click',function(){
    //     $("#guest-login").show();
    // });
</script>
