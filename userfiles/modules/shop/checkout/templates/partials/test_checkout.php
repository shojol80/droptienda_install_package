<style>
    .checkout-heading{
        margin-bottom: 30px;
    }
    .checkout-heading p a{
        display: inline-block;
        color: #5E8334 !important;
    }
    .checkout-form{
        background-color: #F3F0E9;
        padding:15px;
    }

    .checkout label{
        font-weight: normal;
    }

    .ems-shipping,
    .paypal,
    .credit-card{
        display:flex;
        justify-content:space-between;
        align-items:center;
        padding:10px;
    }

    .shipping-sys-inner,
    .payment-sys-inner{
        background-color: #F3F0E9;
    }

    .shipping-sys img,
    .payment-sys img{
        height: 20px;
        width: 40px;
    }

    .free-shipping{
        padding:10px;
        border-bottom:1px solid #ccc;
    }
    .payment-sys{
        margin-top:30px;
    }

    .paypal{
        border-bottom:1px solid #ccc;
    }

    .heading{
        text-transform: uppercase;
        font-weight:700;
    }

    .order-description{
        background-color: #F3F0E9;
        padding:15px;
        margin-bottom:10px;
    }
    .order-button{
        text-align:right;
    }
    .order-button button{
        text-transform: uppercase;
        background-color: #F6A900;
        color: #fff;
        padding-left: 20px;
        padding-right: 20px;
    }
</style>

<section class="checkout">
    <div class="container">
        <div class="checkout-heading">
            <h1>Checkout</h1>
            <p>Fill the form below to complete your purchase!</p>
            <p>
                <a href="">Already registered?</a>
                <a href="">Click here to login.</a>
            </p>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h4 class="heading">
                    1.Billing Address
                </h4>
                <div class="checkout-form">
                    <form action="">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>First Name *</label>
                                    <input type="text" class="form-control" placeholder="Jane">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last Name *</label>
                                    <input type="text" class="form-control" placeholder="Doe">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email Address *</label>
                                    <input type="eamil" class="form-control" placeholder="abc@etc.com">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Telephone *</label>
                                    <input type="text" class="form-control" placeholder="+1801-562-999">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Address *</label>
                                    <input type="text" class="form-control" style="margin-bottom:10px">
                                    <input type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Country *</label>
                                    <select class="form-control">
                                        <option selected>United States</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>City *</label>
                                    <input type="text" class="form-control" placeholder="New York">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Zip/Postal Code *</label>
                                    <input type="text" class="form-control" placeholder="10012">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>State/Province *</label>
                                    <select class="form-control">
                                        <option selected>United States</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Company</label>
                                    <input type="text" class="form-control" placeholder="abc">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Fax</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="">
                                        Create an account for later use
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="">
                                        Create an account for later use
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">

                <div class="shipping-sys">
                    <h4 class="heading">
                        2.Shipping Method
                    </h4>
                    <div class="shipping-sys-inner">
                        <form action="">
                            <div class="free-shipping">
                                <div class="radio">
                                    <label>
                                        <input type="radio" value="option1">
                                        Free Shipping <b>$0.00</b>
                                    </label>
                                </div>
                            </div>
                            <div class="ems-shipping">
                                <div class="radio">
                                    <label>
                                        <input type="radio" value="option2">
                                        EMS(Express Mail Service) : <b>$18.00</b>
                                    </label>
                                </div>
                                <div class="ems-shipping-logo">
                                    <img src="<?php print template_url(); ?>image/ems-logo.jpg" alt="">
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="payment-sys">
                    <h4 class="heading">
                        3.Payment Method
                    </h4>
                    <div class="payment-sys-inner">
                        <form action="">
                            <div class="paypal">
                                <div class="radio">
                                    <label>
                                        <input type="radio" value="">
                                        Paypal
                                    </label>
                                </div>
                                <div class="paypel-logo">
                                    <img src="<?php print template_url(); ?>image/paypal-logo.png" alt="">
                                </div>
                            </div>
                            <div class="credit-card">
                                <div class="radio">
                                    <label>
                                        <input type="radio" value="">
                                        Credit Card
                                    </label>
                                </div>
                                <div class="ems-shipping-logo">
                                    <img src="<?php print template_url(); ?>image/mastercard-logo.jpg" alt="">
                                    <img src="<?php print template_url(); ?>image/visa-logo.png" alt="">
                                    <img src="<?php print template_url(); ?>image/mastercard-logo.jpg" alt="">
                                    <img src="<?php print template_url(); ?>image/visa-logo.png" alt="">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <h4 class="heading">
                    4.Review your order
                </h4>
                <div class="order-description">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col" style="width:38%">Product</th>
                                <th scope="col" class="text-center">Qty</th>
                                <th scope="col" class="text-right">Subtotal</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Bnot Crono 3pf 7050 110nm</td>
                                <td class="text-center">1</td>
                                <td class="text-right">$1950.00</td>
                            </tr>

                            <tr>
                                <td></td>
                                <td>
                                    <p>Subtotal</p>
                                    <p>Shipping</p>
                                    <p><b>Grand Total:</b></p>


                                </td>
                                <td class="text-right">
                                    <p>$1950.00</p>
                                    <p>$18.00</p>
                                    <p><b>$1968.00</b></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <form action="">
                    <div class="form-group">
                        <label>Coupon Code:</label>
                        <input type="text" class="form-control">
                        <button class="btn btn-default">Apply Coupon</button>
                    </div>
                    <div class="form-group">
                        <label>Gift Card Code:</label>
                        <input type="text" class="form-control">
                        <button class="btn btn-default">Apply giftcard</button>
                    </div>
                    <div class="form-group">
                        <label>Comments</label>
                        <textarea name="" id="" cols="30" rows="5" class="form-control" placeholder="Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla minima eos aut aliquam explicabo nobis sint sunt veniam illo,"></textarea>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="">
                                Add a gift massage to my order
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="">
                                Subscribe to our newsletter
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>How did you hear about us?</label>
                        <select class="form-control">
                            <option selected>Other</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                        <label>Please specify</label>
                        <textarea name="" id="" cols="30" rows="2" class="form-control" placeholder="Colleagues"></textarea>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="">
                                I accept the Terms and Conditions
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="">
                                I accept the Terms and Conditions
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="">
                                I accept the Terms and Conditions
                            </label>
                        </div>
                    </div>
                    <div class="order-button">
                        <button class="btn">Place order now</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
