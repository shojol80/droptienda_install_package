<?php

/*

type: layout

name: Select

description: Select country

*/
?>

<style>
  .mw-ui-field-holder select {
    width: 50%;
    /* margin-left: auto; */
}
@media (max-width: 425px){
  .mw-ui-field-holder select{
    width: 90%;
  }
}
</style>
<?php  $selected_country = mw()->user_manager->session_get('shipping_country'); ?>

<div class="mw-ui-field-holder <?php print $config['module_class'] ?>" id="<?php print $rand; ?>">
  <select name="country" class="mw-ui-field element-block shipping-country-select form-control">
   <option value=""><?php _e("Land auswählen"); ?></option>
    <?php foreach($data  as $item): ?>
    <option value="<?php print $item['shipping_country'] ?>"  <?php if(isset($selected_country) and $selected_country == $item['shipping_country']): ?> selected="selected" <?php endif; ?>><?php print $item['shipping_country'] ?></option>
    <?php endforeach ; ?>
  </select>
</div>