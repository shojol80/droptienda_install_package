 
<?php

$paypal_is_test = (get_option('paypalexpress_testmode', 'payments')) == 'y';

?>

<div>
    <p class="alert alert-warning"><small><strong> *<?php _e("Hinweis"); ?> </strong><?php _e("Ihr Warenkorb wird geleert, wenn Sie die Bestellung abschließen"); ?></small> </p>
</div>




<?php if($paypal_is_test == true and is_admin()): ?>
 <?php print notif("You are using Paypal Express in test mode!"); ?>
<?php endif; ?>