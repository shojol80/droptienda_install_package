<?php
/*

  type: layout

  name: Small Modal

  description: Small Modal

 */
?>

<style>
    .dropdown-menu.shopping-cart {
        min-width: 25rem;
    }


</style>

<?php $total = cart_sum(); ?>

<?php if (is_array($data)) : ?>
    <div class="products">
        <?php foreach ($data as $item) : ?>
            <div class="row product-item">
                <div class="col-xs-3 item-img">
                    <?php if (isset($item['item_image']) and $item['item_image'] != false): ?>
                        <?php $p = $item['item_image']; ?>
                    <?php else: ?>
                        <?php $p = get_picture($item['rel_id']); ?>
                    <?php endif; ?>

                    <?php if ($p != false): ?>
                        <img src="<?php print thumbnail($p, 70, 70, true); ?>" alt=""/>
                    <?php endif; ?>
                </div>
                <div class="col-xs-7">
                    <div class="row">
                        <div class="col-md-12 item-title">
                            <a class="" title="" href="<?php print $item['url'] ?>"><?php print $item['title'] ?></a> <span class="text-small pl-1">x<?php print $item['qty'] ?></span>
                        </div>

                        <div class="col-md-12 d-flex item-price">
                            <span><?php print currency_format($item['price']); ?></span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-2 item-action">
                <a title="<?php _e("Remove"); ?>" class="icon-trash" href="javascript:mw.cart.remove('<?php print $item['id'] ?>');"><i class="fa fa-trash" aria-hidden="true"></i></a>
                </div>

            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<?php if (is_ajax()) : ?>

    <script>
        $(document).ready(function () {
            //  cartModalBindButtons();

        });
    </script>

<?php endif; ?>

<div class="products-amount">
    <div class="row">
        <?php if (is_array($data)): ?>
            <div class="col-sm-6 total">
                <p><strong><?php _e("Total Amount: "); ?> <br class="d-none d-sm-block"> <?php print currency_format($total); ?></strong></p>
            </div>
            <div class="col-sm-6" style="text-align: right;">
                <a href="<?php echo site_url('checkout') ?>" class="btn btn-primary btn-md btn-block">Checkout</a>
            </div>
        <?php else: ?>
            <div class="col-12">
                <p><?php _e("Your cart is empty."); ?></p>
            </div>
        <?php endif; ?>
    </div>

</div>

