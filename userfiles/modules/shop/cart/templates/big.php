<?php

/*

type: layout

name: Big

description: Full width cart template

*/

?>

<div class="mw-cart mw-cart-big mw-cart-<?php print $params['id'] ?> <?php print  $template_css_prefix; ?>">
    <div class="mw-cart-title mw-cart-<?php print $params['id'] ?>">
        <h3 style="font-size: 14px;font-weight: bold;text-transform: uppercase;" class="edit">
            Mein Warenkorb
        </h3>
    </div>
    <?php

if(isset($_GET['slug'])) {

    $pro_ids = DB::table('quick_checkout')->where('slug', '=', $_GET['slug'])->first()->products_id;

    $data = \App\Models\Content::with('media','contentData','customField','categoryItem')
        ->where('content.id','=',$pro_ids)
        ->first()->toArray();

//dd($data['media']);
    $price = DB::table('custom_fields')
        ->join('custom_fields_values', 'custom_fields.id', '=', 'custom_fields_values.custom_field_id')
        ->where('custom_fields.rel_id', '=', $pro_ids)
        ->where('custom_fields.rel_type', '=', 'content')
        ->where('custom_fields.type', '=', 'price')
        ->first();
//        dd($price-);


}

    if (is_array($data)) : ?>
    <table
        class="table table-bordered table-striped mw-cart-table mw-cart-table-medium mw-cart-big-table table-responsive cart-table">
        <colgroup>
            <!-- <col width="80"> -->
            <!-- <col width="200"> -->
            <!-- <col width="100"> -->
            <?php if(!isset($_GET['slug'])) { ?>
            <!-- <col width="140"> -->
            <?php } ?>
        </colgroup>
        <thead>
            <tr>
                <th style="width:70px"><?php _e("Bild"); ?></th>
                <th class="mw-cart-table-product" style="width:420px"><?php _e("Produktname"); ?></th>
                <?php if(!isset($_GET['slug'])) { ?>
                <th style="width:80px"><?php _e("MENGE"); ?></th>
                <?php } ?>
                <th><?php _e("Preis"); ?></th>
                <th><?php _e("Gesamt"); ?></th>
                <th><?php _e("Löschen"); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
                $total = cart_sum();
                if(!isset($_GET['slug'])){
                    // dd($data);

                foreach ($data as $item) :
                    // dd();
                    // dd(get_content_by_id($item['rel_id']));

                    //$total += $item['price']* $item['qty'];
                    ?>
            <tr class="mw-cart-item mw-cart-item-<?php print $item['id'] ?>">
                <td><?php  if (isset($item['item_image']) and $item['item_image'] != false): ?>
                    <?php $p = $item['item_image']; ?>
                    <?php else:

                                $p = get_picture($item['rel_id']);

                                ?>
                    <?php endif;?>
                    <?php if ($p != false): ?>
                    <img height="70"
                        class="img-polaroid img-rounded mw-order-item-image mw-order-item-image-<?php print $item['id']; ?>"
                        src="<?php print thumbnail($p, 70, 70); ?>" />
                    <?php endif; ?></td>
                <td class="mw-cart-table-product">
                    <p style="font-weight:bold;margin-bottom:10px;word-break: break-word;">
                        <?php print $item['title'] ?>
                        <?php if (isset($item['custom_fields'])): ?>
                        <?php print $item['custom_fields'] ?>
                        <?php endif ?>

                    </p>
                    <p class="mw-cart-table-product-info">
                        <span>
                            <strong>Item Number:</strong> <?php if($item['rel_id'] != null){ print $item['rel_id']; }else{ print "XXX" ;}?>
                        </span>
                        <span>
                            <strong>EAN:</strong> <?php if(get_content_by_id($item['rel_id'])['ean'] != null){ print get_content_by_id($item['rel_id'])['ean']; }else{ print "XXX" ; } ?>
                        </span>
                        <!-- <span>
                            <strong>Brand:</strong> xaoimi
                        </span>
                        <span>
                            <strong>Color:</strong> Red
                        </span> -->
                    </p>
                </td>
                <?php if(!isset($_GET['slug'])) { ?>
                <td><input type="number" min="1" class="input-mini form-control input-sm"
                        value="<?php print $item['qty'] ?>"
                        onchange="mw.cart.qty('<?php print $item['id'] ?>', this.value)" />
                </td>
                <?php } ?>
                <?php /* <td><?php print currency_format($item['price']); ?></td> */ ?>
                <td class="mw-cart-table-price"><?php print currency_format($item['price']); ?></td>

                <td class="mw-cart-table-price"><?php print currency_format($item['price'] * $item['qty']); ?></td>

                <td style="text-align:center;"><a title="<?php _e("Remove"); ?>" style="color:red;" class="icon-trash"
                        href="javascript:mw.cart.remove('<?php print $item['id'] ?>');"><i class="fa fa-trash"
                            aria-hidden="true"></i></a></td>
            </tr>
            <?php endforeach;
                }else{?>


            <tr class="mw-cart-item mw-cart-item-<?php print $data['id'] ?>">
                <td><?php  if (isset($data['item_image']) and $data['item_image'] != false): ?>
                    <?php $p = $data['item_image']; ?>
                    <?php else:

                                    $p = get_picture($data['id']);
                                ?>
                    <?php endif;
                            //                            @dump($p);?>
                    <?php if ($p != false): ?>
                    <img height="70"
                        class="img-polaroid img-rounded mw-order-item-image mw-order-item-image-<?php print $data['id']; ?>"
                        src="<?php print thumbnail($p, 70, 70); ?>" />
                    <?php endif; ?></td>
                <td class="mw-cart-table-product"><?php print $data['title'] ?>
                    <?php if (isset($data['custom_fields'])): ?>
                    <?php print $data['custom_fields'] ?>
                    <?php endif ?></td>



                <td class="mw-cart-table-price"><?php print currency_format($price->value); ?></td>

            </tr>

            <?php } ?>

        </tbody>
    </table>
    <div style="margin-top:20px;" class="checkout-checkbox">
        <?php $terms=true; $terms2=true; $terms3=true; ?>
        <?php if ($terms): ?>
        <script>
            $(document).ready(function () {

                terms('#i_agree_with_terms_1');
            });

            function terms(id) {
                $(id).click(function () {
                    let el = $('#i_agree_with_terms_1');
                    let el2 = $('#i_agree_with_terms_2');
                    let el3 = $('#i_agree_with_terms_3');
                    if (el.is(':checked') && el2.is(':checked')) {
                        $('#complete_order_button').removeAttr('disabled');
                    } else {
                        $('#complete_order_button').attr('disabled', 'disabled');

                    }
                });
            }

        </script>
        <div style="display:flex;justify-content:flex-end;" id="i_agree_with_terms_row">

                <div>
                    <span class="edit" field="content1" rel="page1"><?php _e('Ich akzeptiere die'); ?></span>
                    <a href="<?php print site_url('terms-and-conditions-first') ?>" target="_blank" style="color:#309be3">Allgemeinen Geschäftsbedingungen</a>
                </div>

        </div>
        <?php endif; ?>


        <?php if ($terms2): ?>
        <script>
            $(document).ready(function () {
                terms('#i_agree_with_terms_2');
            });

        </script>
        <div style="display:flex;margin-top:10px;justify-content:flex-end;" id="i_agree_with_terms_row">
                <div class="ch-checkbox">
                    <span class="edit" field="content2" rel="page2"><?php _e('Ich habe das'); ?></span>
                    <a href="<?php print site_url('widerrufsrecht') ?>" target="_blank" style="color:#309be3">Widerrufsrecht zur
                        Kenntnis genommen</a>
                </div>
        </div>
        <?php endif; ?>
        <!-- <?php if ($terms3): ?>
                                                <script>
                                                    $(document).ready(function () {
                                                        terms('#i_agree_with_terms_3');
                                                    });

                                                </script>

                                                <div class="mw-ui-row" id="i_agree_with_terms_row">
                                                    <label class="mw-ui-check">
                                                        <input type="checkbox" name="terms" id="i_agree_with_terms_3" value="1"
                                                            autocomplete="off" />
                                                        <span></span>
                                                        <span>
                                                            <?php _e('I agree with the third'); ?>
                                                            <a href="<?php print site_url('terms-and-condition-three') ?>"
                                                                target="_blank"><?php _e('Terms and Conditions'); ?></a>
                                                        </span>
                                                    </label>
                                                </div>
                                                <?php endif; ?> -->

    </div>

    <?php $shipping_options = mw('shop\shipping\shipping_api')->get_active(); ?>
    <?php

        $show_shipping_info = get_option('show_shipping', $params['id']);

        if ($show_shipping_info === false or $show_shipping_info == 'y') {
            $show_shipping_stuff = true;
        } else {
            $show_shipping_stuff = false;
        }

        if (is_array($shipping_options)) :?>

    <h3 style="font-size: 14px;font-weight: bold;text-transform: uppercase;" class="edit">
        Bestellübersicht
    </h3>
    <table cellspacing="0" cellpadding="0"
        class="table table-responsive table-bordered table-striped mw-cart-table mw-cart-table-medium checkout-total-table"
        width="100%">
        <style scoped="scoped">
            .checkout-total-table {
                table-layout: fixed;
            }

            .checkout-total-table label {
                display: block;
                text-align: right;
            }

            .cell-shipping-total,
            .cell-shipping-price {
                text-align: right;
            }

            .total_cost {
                font-weight: normal;
            }

        </style>
        <!-- <col width="">
                    <col width="">
                    <col width=""> -->
        <tbody>
            <tr <?php if (!$show_shipping_stuff) : ?> style="display:none" <?php endif; ?>>
                <td class="cell-shipping-country"><label>
                        <?php _e("Versand nach"); ?>
                        :</label></td>
                <td class="cell-shipping-country">
                    <module type="shop/shipping" view="select" />
                </td>
            </tr>
            <tr>
                <td><label>
                        <?php _e("Versandkosten"); ?>
                        :</label></td>
                <td class="cell-shipping-price">
                    <div class="mw-big-cart-shipping-price" style="display:inline-block">
                        <module type="shop/shipping" view="cost" />
                    </div>
                </td>
            </tr>







            <?php if ($cart_totals && !isset($_GET['slug'])) { ?>
            <?php  foreach ($cart_totals as $cart_total_key => $cart_total_item) { ?>
            <?php if ($cart_total_key != 'shipping') { ?>
            <tr>

                <td><label class="">
                        <?php if($cart_total_item['label'] == 'Subtotal'){ print "Zwischensumme"; }
                                                elseif($cart_total_item['label'] == 'Total'){ print "Summe"; }
                                                elseif($cart_total_item['label'] == 'Tax'){ print "MwSt"; }?>
                        :</label></td>
                <td class="cell-shipping-price cell-cart-price-total-<?php print $cart_total_key; ?>">
                    <?php print $cart_total_item['amount']; ?></td>
            </tr>
            <?php } ?>
            <?php } ?>
            <?php } ?>


        </tbody>
    </table>



    <?php endif;
        ?>
    <?php
        if (!isset($params['checkout-link-enabled'])) {
            $checkout_link_enanbled = get_option('data-checkout-link-enabled', $params['id']);
        } else {
            $checkout_link_enanbled = $params['checkout-link-enabled'];
        }
        ?>
    <?php if ($checkout_link_enanbled != 'n') : ?>
    <?php $checkout_page = get_option('data-checkout-page', $params['id']); ?>
    <?php if ($checkout_page != false and strtolower($checkout_page) != 'default' and intval($checkout_page) > 0) {
                $checkout_page_link = content_link($checkout_page) . '/view:checkout';
            } else {
                $checkout_page_link = site_url('checkout');
            }
            ?>
    <a class="btn  btn-warning pull-right" href="<?php print $checkout_page_link; ?>">
        <?php _e("Checkout"); ?>
    </a>
    <?php endif; ?>
    <?php else : ?>
    <h4 class="alert alert-warning">
        <?php _e("Noch keine Artikel im Warenkorb"); ?>
    </h4>
    <?php endif;
    ?>
</div>
<?php if(isset($_GET['slug'])){?>
<script>
    // if(sessionStorage.getItem("quick_cart") != "cart") {
    $(document).ready(function () {
        var i = 0;
        // const urlParams = new URLSearchParams(window.location.search);
        // console.log(sessionStorage.getItem("quick_cart"))

        mw.cart.add_item('<?php print $data["id"] ?>');
        // sessionStorage.setItem("quick_cart", "cart");

    });
    // }

</script>
<?php } ?>
