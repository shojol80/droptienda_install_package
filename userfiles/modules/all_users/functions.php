<?php
	event_bind('mw.user.after_register', function() {
		    //setup the request, you can also use CURLOPT_URL
		$ch4 = curl_init(config('global.drm_base_url').'api/dorptinda-new-customer-notify');

		// Returns the data/output as a string instead of raw data
		curl_setopt($ch4, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch4, CURLOPT_CUSTOMREQUEST, "GET");
		// get stringified data/output. See CURLOPT_RETURNTRANSFER
		$data = curl_exec($ch4);
		// get info about the request
		$info = curl_getinfo($ch4);
		// close curl resource to free up system resources
		curl_close($ch4);
		dd($data);
		die();

	});

api_expose('all_users');

function all_users($params=false){
	
	$onlyAllUsers = DB::table('users')->where('is_admin',NULL)->get();
	return ($onlyAllUsers);

}


function userDelete($params=false){
	$userid = Auth::id();
	$userInActive = DB::table('users')->where('id',$userid)->update(['is_active' => 0]);
	return $userInActive ;

}

function admin($params=false){
	
	$admin = DB::table('users')->where('is_admin',1) ->pluck('is_admin')->get();
	return $admin ;

}

