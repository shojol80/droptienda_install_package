<?php
$config = array();
$config['name'] = "Dynamic Banners";
$config['description'] = "Dynamic Banners for promoting products";
$config['author'] = "Md Rashedul Islam";
$config['ui'] = false; //you can drop this module in live edit
$config['ui_admin'] = true; //your module is visible in the admin
$config['position'] = "98";
$config['version'] = "0.01";
