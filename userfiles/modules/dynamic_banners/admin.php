<div class="mw-module-admin-wrap">
    <div class="module module-admin-modules-info" id="back-button">
        <div class="section-header">
            <a title="Back" onclick="showBannerList()"
               class="mw-ui-btn mw-ui-btn-info mw-ui-btn-medium m-l-10 btn-back">
                <i class="mw-icon-arrowleft"></i> &nbsp;
                <span>Back</span>
            </a>
        </div>
    </div>

    <div id="users-admin">

        <div class="admin-side-content">

            <div class="mw-ui-row-nodrop pull-left" style="width: auto">
                <div class="mw-ui-col">
                    <div class="mw-col-container"><h2 id="user-section-title">Manage banners</h2></div>
                </div>
                <div class="mw-ui-col" id="add-banner-button">
                    <div class="mw-col-container">
                        <a href="#create-banner" onclick="createBannerShow()"
                           class="mw-ui-btn mw-ui-btn-medium mw-ui-btn-notification mw-ui-btn-rounded">
                            <i class="fas fa-plus-circle"></i>
                            &nbsp;<span>Add new banner</span>
                        </a>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(() => {
                    createBannerShow();
                });

                function createBannerShow() {
                    $("#back-button").show();
                    $("#create-banner").show();
                    $("#table-list-show").hide();
                    $("#add-banner-button").hide();
                }

                function showBannerList() {
                    $("#create-banner").hide();
                    $("#table-list-show").show();
                    $("#back-button").hide();
                    $("#add-banner-button").show();
                }
            </script>
            <div class="mw-simple-rotator" id="table-list-show">
                <div class="mw-simple-rotator-container activated" id="mw-users-manage-edit-rotattor"
                     data-state="1">
                    <div class="mw-simple-rotator-item module module module-users-manage "
                         contenteditable="false" style="">
                        <div class="table-responsive">
                            <table class="mw-ui-table table-style-2 layout-auto" width="100%">
                                <thead>
                                <tr>
                                    <th>Banner Title</th>
                                    <th>Banner Image</th>
                                    <th>Associated Products</th>
                                    <th>Edit</th>
                                </tr>
                                </thead>

                                <tbody>
                                <tr id="mw-admin-user-4">
                                    <td>
                                        <span class="mw-user-thumb mw-user-thumb-small mw-icon-user"></span>
                                        <span class="mw-user-names">Fast User</span>
                                    </td>
                                    <td>user@gmail.com</td>
                                    <td>user@gmail.com</td>
                                    <td>
                                        <a class="show-on-hover mw-ui-btn mw-ui-btn-info mw-ui-btn-outline mw-ui-btn-small"
                                           href="#">Edit</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mw-ui-box module-users-edit-user" id="create-banner">
                <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
                <link rel="stylesheet"
                      href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.1.3/darkly/bootstrap.min.css">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
                <div class="mw-ui-box-header" style="margin-bottom: 0;">
                    <span class="ico iu  rs"></span>
                    <span>Add new banner</span>
                </div>

                <div>
                    <table class="mw-ui-table mw-ui-table-basic mw-admin-user-tab-content">
                        <tbody>
                        <!--<tr>
                            <td><label class="mw-ui-label">Banner Image</label></td>
                            <td>
                                <p>

                                </p>
                                <p>

                                </p>
                                <img id="beatles" style="width:400px; height:240px;" usemap="#beatles-map" alt=""
                                     src="http://droptienda.localhost/userfiles/media/uploads/f64a2c3ccbd185099116875119download.jpeg">
                                <map name="beatles-map">
                                    <area shape="rect" data-name="paul,all" coords="0,0,380,228" href="#" alt="">
                                    <area shape="rect" data-name="ringo,all" coords="0,670,380,898" href="#" alt="">
                                    <area shape="rect" data-name="john,all" coords="1218,670,1599,898" href="#" alt="">
                                    <area shape="rect" data-name="george,all" coords="1218,0,1599,228" href="#" alt="">
                                </map>
                                <div id="banner_caption_area"></div>
                                <script src="http://droptienda.localhost/userfiles/media/uploads/jquery.imagemapster.js"
                                        crossorigin="anonymous"></script>




                                <script>
                                    var inArea,
                                        map = $('#beatles'),
                                        captions = {
                                            paul: ["Paul McCartney - Bass Guitar and Vocals",
                                                "Paul McCartney's song, Yesterday, recently voted the most popular song "
                                                + "of the century by a BBC poll, was initially composed without lyrics. "
                                                + "Paul used the working title 'scrambled eggs' before coming up with the final words."],
                                            ringo: ["Ringo Starr - Drums",
                                                "Dear Prudence was written by John and Paul about Mia Farrow's sister, Prudence, "
                                                + "when she wouldn't come out and play with Mia and the Beatles at a religious retreat "
                                                + "in India."],
                                            john: ["John Lennon - Guitar and Vocals",
                                                "In 1962, The Beatles won the Mersyside Newspaper's biggest band in Liverpool "
                                                + "contest principally because they called in posing as different people and voted "
                                                + "for themselves numerous times."],
                                            george: ["George Harrison - Lead Guitar and Vocals",
                                                "The Beatles' last public concert was held in San Francisco's Candlestick "
                                                + "Park on August 29, 1966."]
                                        },
                                        single_opts = {
                                            fillColor: '000000',
                                            fillOpacity: 0,
                                            stroke: true,
                                            strokeColor: 'ff0000',
                                            strokeWidth: 2
                                        },
                                        all_opts = {
                                            fillColor: 'ffffff',
                                            fillOpacity: 0.6,
                                            stroke: true,
                                            strokeWidth: 2,
                                            strokeColor: 'ffffff'
                                        },
                                        initial_opts = {
                                            mapKey: 'data-name',
                                            isSelectable: false,
                                            onMouseover: function (data, e) {
                                                let mX = parseFloat(data.e.pageX) + 100;
                                                let mY = parseFloat(data.e.pageY) + 100;

                                                inArea = true;
                                                $('#beatles-caption-header').text(captions[data.key][0]);
                                                $('#beatles-caption-text').text(captions[data.key][1]);

                                                $('#banner_caption_area').append(`
                                                    <div id="banner_caption" style="font-style: italic; font-weight: bold; margin-bottom: 12px; top : ${mY}px; left: '${mX}px'; position : fixed; z-index : 9999999;">
                                                        <p>${captions[data.key][0]}</p>
                                                        <p>${captions[data.key][1]}</p>
                                                    </div>
                                                `);
                                            },
                                            onMouseout: function (data) {
                                                inArea = false;
                                                // $('#beatles-caption').hide();
                                                $('#banner_caption').remove();
                                            }
                                        };
                                    opts = $.extend({}, all_opts, initial_opts, single_opts);

                                    map.mapster('unbind')
                                        .mapster(opts)
                                        .bind('mouseover', function () {
                                            if (!inArea) {
                                                map.mapster('set_options', all_opts)
                                                    .mapster('set', true, 'all')
                                                    .mapster('set_options', single_opts);
                                            }
                                        }).bind('mouseout', function () {
                                        if (!inArea) {
                                            map.mapster('set', false, 'all');
                                        }
                                    });
                                </script>

                            </td>
                        </tr>-->

                        <!-- <tr>


                             <td>

                                 <div class="row">
                                     <div class="col-xs-12">
                                         <h3>Demo 1：No Resize</h3>
                                         <div class="imagemaps-wrapper">
                                             <img
                                                 src="http://droptienda.localhost/userfiles/media/uploads/f64a2c3ccbd185099116875119download.jpeg"
                                                 draggable="false">
                                         </div>
                                         <div class="imagemaps-control">
                                             <fieldset>
                                                 <legend>Settings</legend>
                                                 <table class="table table-hover">
                                                     <thead>
                                                     <tr>
                                                         <th scope="col">#</th>
                                                         <th scope="col">Links</th>
                                                         <th scope="col">Target</th>
                                                         <th scope="col">Delete</th>
                                                     </tr>
                                                     </thead>
                                                     <tbody class="imagemaps-output">
                                                     <tr class="item-###">
                                                         <th scope="row">###</th>
                                                         <td><input type="text" class="form-control area-href"></td>
                                                         <td>
                                                             <select class="form-control area-target">
                                                                 <option value="_self">_self</option>
                                                                 <option value="_blank">_blank</option>
                                                             </select>
                                                         </td>
                                                         <td>
                                                             <button type="button" class="btn btn-danger btn-delete">
                                                                 Delete
                                                             </button>
                                                         </td>
                                                     </tr>
                                                     </tbody>
                                                 </table>
                                             </fieldset>
                                             <div>
                                                 <button type="button" class="btn btn-info btn-add-map">Add New</button>
                                                 <button type="button" class="btn btn-success btn-get-map">Get Code
                                                 </button>
                                             </div>
                                         </div>
                                     </div>
                                 </div>


                             </td>
                         </tr>-->

                        <tr>
                            <td>
                                <div class="row">
                                    <style type="text/css">
                                        .imagemaps-wrapper {
                                            display: flex;
                                            justify-content: center;
                                            align-items: center;
                                            position: relative;
                                        }

                                        .imagemaps-wrapper img {
                                            max-width: 100%;
                                        }

                                        .imagemaps-control {
                                        }
                                    </style>
                                    <div class="col-xs-12">
                                        <input type="file" accept="image/*" name="image" id="file"
                                               onchange="loadFile(event)" style="display: none;">
                                        <label for="file" style="cursor: pointer;">Upload Image</label>

                                        <div class="imagemaps-wrapper">
                                            <img
                                                src="http://droptienda.localhost/userfiles/media/uploads/f64a2c3ccbd185099116875119download.jpeg"
                                                draggable="false" usemap="#imagemaps"
                                                style="height: 300px; width: 450px">
                                            <map class="imagemaps" name="imagemaps">

                                                <area shape="rect" name="imagemaps-area" class="imagemaps-area0" coords="0,0,194,116" href="" target="_self">
                                                <area shape="rect" name="imagemaps-area" class="imagemaps-area1" coords="0,952,192,1066" href="" target="_self">
                                                <area shape="rect" name="imagemaps-area" class="imagemaps-area2" coords="1404,952,1600,1066" href="" target="_self">
                                                <area shape="rect" name="imagemaps-area" class="imagemaps-area3" coords="821,419,1016,739" href="" target="_self">

                                            </map>
                                        </div>
                                        <div class="imagemaps-control">
                                            <fieldset>
                                                <legend>Settings</legend>
                                                <table class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Links</th>
                                                        <th scope="col">Target</th>
                                                        <th scope="col">Delete</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="imagemaps-output">
                                                    <tr class="item-###">
                                                        <th scope="row">###</th>
                                                        <td><input type="text" class="form-control area-href"></td>
<!--                                                        <td>-->
<!--                                                            <select class="form-control area-target">-->
<!--                                                                <option value="_self">_self</option>-->
<!--                                                                <option value="_blank">_blank</option>-->
<!--                                                            </select>-->
<!--                                                        </td>-->
                                                        <td>
                                                            <button type="button" class="btn btn-danger btn-delete">
                                                                Delete
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>
                                            <div>
                                                <button type="button" class="btn btn-info btn-add-map">Add New</button>
                                                <button type="button" class="btn btn-success btn-get-map">Get Code
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"
        src="http://droptienda.localhost/userfiles/media/uploads/jquery.imagemaps.js"></script>
<script>
    let imageId = '';
    /** IMAGE UPLOAD WHILE ON SELECT */
    const loadFile = event => {
        const image = document.getElementById('img');
        const formData = new FormData();
        formData.append('file', event.target.files[0]);

        $.ajax({
            url: '<?php /*print api_url('dynamic_banners_create') */?>',
            type: 'POST', data: formData, processData: false, contentType: false,
            success: data => {
                image.src = '<?php /*echo site_url()*/?>' + data.filename;
                imageId = data.id;
            }
        });
    }
</script>
<script type="text/javascript">
    $('.imagemaps-wrapper').imageMaps({
        addBtn: '.btn-add-map',
        output: '.imagemaps-output',
        stopCallBack: function (active, coords) {
        }
    });

    $('.btn-get-map').on('click', function () {
        let oParent = $(this).parent().parent().parent();
        let result = oParent.find('.imagemaps-wrapper').clone();
        result.children('div').remove();
        var children = result.children();
        const result2 = children[1];
        console.log(result.html())
        const allAreas = result2.areas;
        const coords = [];
        for (let i = 0; i < allAreas.length; i++) {
            const ar = allAreas[i];
            coords.push({coords: ar.coords, product: ar.href});
        }
        updateBannerCoords(coords)
        // console.log(coords)
    });
</script>
<script>
    /** MAP UPDATE WHILE ON SUBMIT */
    const updateBannerCoords = (coords) => {
        // console.log(coords)

        $.ajax({
            url: '<?php print api_url('dynamic_banners_create_maps') ?>',
            type: 'POST', data: {coords: coords, imageId},
            success: data => {
                // console.log(data)
            }
        });
    }
</script>
