<?php

use Illuminate\Support\Facades\DB;

api_expose("dynamic_banners_create");
api_expose("dynamic_banners_create_maps");

/**
 * ========================================
 * API FOR UPLOAD FILE FOR BANNERS
 * ========================================
 */
function dynamic_banners_create()
{
    $response = [];
    if (is_logged())
    {
        $file = $_FILES["file"];
        $filetype = $file["type"];
        $allowed_extensions = ["gif", "jpeg", "jpg", "png"];
        $temporary = explode(".", $file["name"]);
        $extension = end($temporary);
        $max_size = 200000;
        $unique = str_shuffle(uniqid() . time() . rand(100, 1000));

        if (in_array($extension, $allowed_extensions))
        {
            if ($filetype == "image/gif" || $filetype == "image/jpeg" || $filetype == "image/jpg" || $filetype == "image/pjpeg" || $filetype == "image/png" || $filetype == "image/x-png")
            {
                if ($file["size"] < $max_size)
                {
                    move_uploaded_file($file["tmp_name"], "userfiles/media/uploads/" . $unique . $file["name"]);
                    $mediaId = createImage(user_id(), "userfiles/media/uploads/" . $unique . $file["name"]);
                    $media = getSingleMedia($mediaId);
                    $response["message"] = $media;
                    $response["status"] = 200;
                } else
                {
                    $response["message"] = "File max limit crossed.";
                    $response["status"] = 421;
                }
            } else
            {
                $response["message"] = "Invalid file";
                $response["status"] = 422;
            }
        } else
        {
            $response["message"] = "Invalid file";
            $response["status"] = 422;
        }
    } else
    {
        $response["message"] = "You are not logged in";
        $response["status"] = 401;
    }

    return response()->json($response["message"], $response["status"]);
}

/**
 * @param $userId integer
 * @param string $filename
 * @return integer
 *
 * @noinspection PhpUndefinedMethodInspection
 */
function createImage(int $userId, string $filename)
{
    $currentTime = Carbon\Carbon::now();
    return DB::table("media")->insertGetId([
        "updated_at" => $currentTime->toDateTimeString(),
        "created_at" => $currentTime->toDateTimeString(),
        "created_by" => $userId,
        "edited_by" => $userId,
        "session_id" => null,
        "rel_type" => "banner",
        "rel_id" => null,
        "media_type" => "picture",
        "position" => 9999999,
        "title" => null,
        "description" => null,
        "embed_code" => null,
        "filename" => $filename,
        "image_options" => null,
    ]);
}

/**
 * @param $id integer
 * @return array
 * @noinspection PhpUndefinedMethodInspection
 */
function getSingleMedia(int $id)
{
    return DB::table("media")->where("id", $id)->get()->first();
}

function dynamic_banners_create_maps()
{
    return response()->json($_REQUEST, 200);
}
