<div class="mw-user-logged-holder">
    <div class="mw-user-welcome">
        <?php _e("Herzlich willkommen"); ?>
        <?php print user_name(); ?> </div>
    <a href="<?php print site_url() ?>">
        <?php _e("Gehe zu"); ?>
        <?php print site_url() ?></a><br/>
    <a href="<?php print api_link('logout') ?>" class="btn btn-danger" style="margin:10px 0">
        <?php _e("Ausloggen"); ?>
    </a>
    <?php if (is_admin()): ?>
        <div class=""><a href="<?php print admin_url() ?>" class="btn btn-primary">
                <?php _e("Administrationsmenü"); ?>
            </a></div>
    <?php endif; ?>
</div>
