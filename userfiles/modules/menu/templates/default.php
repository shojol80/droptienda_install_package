<?php

/*

type: layout

name: Default

description: Default Menu skin

*/

?>

<script>mw.moduleCSS("<?php print $config['url_to_module']; ?>style.css", true);</script>

<?php
$menu_filter['ul_class'] = 'main-nav';
?>

<div class="module-navigation module-navigation-default responsive-bg" id="cssmenu">
      <?php
      $mt =  menu_tree($menu_filter, false, true);
      if($mt != false){
        print ($mt);
      }
      else {
        print lnotif(_e('There are no items in the menu', true) . " <b>".$params['menu-name']. '</b>');
      }
      ?>
</div>
