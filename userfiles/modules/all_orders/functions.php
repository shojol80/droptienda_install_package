<?php

use App\Models\Order;

api_expose('all_orders');

function all_orders($params = false)
{
    return Order::has('carts')->with('carts')->get();
}



