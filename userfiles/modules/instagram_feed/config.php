<?php

$config = array();
$config['name'] = "Instagram feed";
$config['description'] = "Feed of instagram images";
$config['author'] = "Petko Yovchevski";
$config['author_website'] = "https://www.plumtex.com";
$config['ui'] = true;
$config['categories'] = "social";
$config['position'] = 13;
$config['version'] = 0.01;
