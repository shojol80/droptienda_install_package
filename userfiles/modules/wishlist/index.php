<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .wishlist {}

        .wishlist h4 {
            color: #309be3;
            font-size: 18px;
            padding: 10px 0;
        }

        .wishlist-list ul {
            padding-left: 20px;
            margin: 0;
            list-style: none;
        }

        .wishlist-list ul li {
            display: block;
        }

        .wishlist-item-text a {
            padding: 10px;
            display: block;
            color: #373737;
            font-size: 14px;
            text-decoration: none;
        }
        .wishlist-item-text a:hover{
            color: #309be3;
        }

        .wishlist-item-button{
            display:flex;
        }
    </style>
</head>

<body>
<div class="sidebar__widget m-b-40">
            <h4 class="m-b-20"><?php _lang("Wishlist"); ?></h4>

            <div class="module module-categories">
                <div class="module-categories module-categories-template-horizontal-list-1">
                    <ul class="mw-cats-menu" id="wishlist-list">

                    </ul>
                </div>
            </div>

            <?php if (is_logged()) { ?>
                <div id="wishlist-sidebar"></div>
                <p>&nbsp;</p>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                    Create new wishlist
                </button>
            <?php } else { ?>
                <button data-toggle="modal" class="btn btn-primary" data-target="#loginModal">Login für Wunschliste</button>
            <?php } ?>
        </div>
</body>

</html>
<script>
    $(document).ready(() => {

$('.js-example-basic-multiple').select2();
var a = $(".select2-selection--single").parent().hide();
a.parent().hide();

$.get(`<?= api_url('get_wishlist_sessions'); ?>`, result => {

    const selected = [];
    const list = [];
    result.forEach(function (session) {
        // console.log(session);
        list.push('<option value=' + session['id'] + '>' + session['name'] + '</option>');

        // $("#wishlist-list").append('<li style="display: block;" title="' + session['name'] + '"><a href="shop?wishlist_id=' + session["id"] + '" data-category-id="' + session['id'] + '" title="' + session['name'] + '" class="depth-0">' + session['name'] + '</a><button type="button" id="delete_sss" class="btn" data-toggle="modal" data-name="'+session['name']+'" ><i class="fa fa-trash-o" aria-hidden="true"></i></button><button type="button" id="edit_sss" class="btn" data-toggle="modal" data-target="#exampleModalCenteredit" data-name="'+session['name']+'" ><i class="fa fa-pencil" aria-hidden="true"></i></button></li>');
        session['products'].forEach(function (prod) {
            if (selected[parseInt(prod['product_id'])] === undefined) {
                selected[parseInt(prod['product_id'])] = [];
            }
            selected[parseInt(prod['product_id'])].push(session.id.toString())
        })
    });

    <?php if (!empty($data)): ?>
    <?php foreach ($data as $item): ?>
    var wishlistProduct = $("#wishlist-select-<?php echo $item['id'];?>");
    wishlistProduct.empty();
    wishlistProduct.append('<option disabled value="null"></option>');
    list.forEach(function (value) {

        wishlistProduct.append(value);

    });
    var didd = <?php echo $item['id'];?>;
    wishlist_details(didd);



    <?php endforeach; ?>
    <?php endif; ?>

    // console.log(result);
    // console.log("selected length => " ,selected.length);
    // console.log(selected);
    // selected products trigger added
    selected.forEach(function (value, index) {
        const wishlistProduct2 = $("#wishlist-select-" + index.toString());
        // console.log("#wishlist-select-" + index.toString())
        wishlistProduct2.select2().val(value).trigger("change");
    });
    function wishlist_details(didd) {
        // console.log(didd)
        if (selected[didd] && selected[didd].length > 0)
            $("#wishlist-heart-"+didd).attr("src","<?php print template_url(); ?>image/icon.png");
        else
            $("#wishlist-heart-"+didd).attr("src","<?php print template_url(); ?>image/heart-blue.png");
    }

});
});

$(document).on('click','#edit_sss', function(){

let name = $(this).data('name');

// console.log(name);
// console.log(id);


$("#exampleInputEmailedit").val(name);
$("#exampleInputEmailedithide").val(name);
});


$(document).on('click','#delete_sss', function(){

let name = $(this).data('name');

// console.log(name);
// console.log(id);
$.post("<?php print api_url('delete_wishlist_sessions'); ?>", {name: name}, function (sessions) {
                if (sessions === 'false') {
                    // emailHelp.show();
                    location.reload();

                } else {
                    location.reload();
                }
            });


});


</script>
