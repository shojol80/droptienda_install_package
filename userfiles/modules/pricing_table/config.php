<?php

$config = array();
$config['name'] = "Pricing Table";

$config['description'] = "Free pricing table module for your website!";
$config['author'] = "Petko Yovchevski";
$config['ui'] = true;
$config['ui_admin'] = false;
$config['categories'] = "other";
$config['position'] = "100";
$config['version'] = 0.01;
