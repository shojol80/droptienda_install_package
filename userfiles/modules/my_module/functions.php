<?php
// content
use Illuminate\Support\Facades\DB;

api_expose('upimprint');
function upimprint($data){

    $inaray= array(
        'term_name' => 'imprint',
        'description' => $data['imprint'],

    );
    DB::table('legals')->where('term_name','imprint')->update(
        array(
            'description' => $data['imprint']
        )
    );
    return back();




}

api_expose('uppp');
function uppp($data){


    $inaray1= array(
        'term_name' => 'pp',
        'description' => $data['pp'],

    );
    DB::table('legals')->where('term_name','pp')->update(
        array(
            'description' => $data['pp']
        )
    );
    return back();




}
api_expose('upagb');
function upagb($data){


    $inaray2= array(
        'term_name' => 'agb',
        'description' => $data['agb'],

    );
    DB::table('legals')->where('term_name','agb')->update(
        array(
            'description' => $data['agb']
        )
    );
    return back();



}
api_expose('upcancle');
function upcancle($data){


    $inaray3= array(
        'term_name' => 'cancle',
        'description' => $data['cancle'],

    );
    DB::table('legals')->where('term_name','cancle')->update(
        array(
            'description' => $data['cancle']
        )
    );
    return back();




}
api_expose('uppayment');
function uppayment($data){


    $inaray4= array(
        'term_name' => 'payment',
        'description' => $data['payment'],

    );
    DB::table('legals')->where('term_name','payment')->update(
        array(
            'description' => $data['payment']
        )
    );
    return back();



}
api_expose('upshipping');
function upshipping($data){


    $inaray7= array(
        'term_name' => 'shipping',
        'description' => $data['shipping'],

    );
    DB::table('legals')->where('term_name','shipping')->update(
        array(
            'description' => $data['shipping']
        )
    );
    return back();




}
api_expose('upinfoo');
function upinfoo($data){


    $inaray5= array(
        'term_name' => 'info',
        'description' => $data['infoo'],

    );
    DB::table('legals')->where('term_name','info')->update(
        array(
            'description' => $data['info']
        )
    );
    return back();




}
api_expose('upnote');
function upnote($data){


    $inaray6= array(
        'term_name' => 'note',
        'description' => $data['note'],

    );
    DB::table('legals')->where('term_name','note')->update(
        array(
            'description' => $data['note']
        )
    );
    return back();




}



api_expose('imprint');
function imprint($data){


    $inaray= array(
        'term_name' => 'imprint',
        'description' => $data['imprint'],

    );
    DB::table('legals')->insert($inaray);
    return back();




}

api_expose('pp');
function pp($data){


    $inaray1= array(
        'term_name' => 'pp',
        'description' => $data['pp'],

    );
    DB::table('legals')->insert($inaray1);
    return back();




}
api_expose('agb');
function agb($data){


    $inaray2= array(
        'term_name' => 'agb',
        'description' => $data['agb'],

    );
    DB::table('legals')->insert($inaray2);
    return back();




}
api_expose('cancle');
function cancle($data){


    $inaray3= array(
        'term_name' => 'cancle',
        'description' => $data['cancle'],

    );
    DB::table('legals')->insert($inaray3);
    return back();




}
api_expose('payment');
function payment($data){


    $inaray4= array(
        'term_name' => 'payment',
        'description' => $data['payment'],

    );
    DB::table('legals')->insert($inaray4);
    return back();




}
api_expose('shipping');
function shipping($data){


    $inaray7= array(
        'term_name' => 'shipping',
        'description' => $data['shipping'],

    );
    DB::table('legals')->insert($inaray7);
    return back();




}
api_expose('infoo');
function infoo($data){


    $inaray5= array(
        'term_name' => 'info',
        'description' => $data['infoo'],

    );
    DB::table('legals')->insert($inaray5);
    return back();




}
api_expose('note');
function note($data){


    $inaray6= array(
        'term_name' => 'note',
        'description' => $data['note'],

    );
    DB::table('legals')->insert($inaray6);
    return back();




}


//api_expose('my_hello_world');
//function my_hello_world($params)
//{
//    print 'hello_world';
//}
