<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .container{
            padding:50px 20px;
        }
        .form-group {
            margin-bottom: 40px;
        }

        .form-group label {
            margin-bottom: 10px;
            display: block;
            font-size: 18px;
            font-weight: 600;
        }

        .form-group button {
            background-color: #42A1E3;
            padding: 8px 15px;
            color: #fff;
            border: 0px;
            border-radius: 5px;
            box-shadow: 0;
            margin-top: 5px;
        }

    </style>
</head>

<body>
    <div class="container">
        <?php if( check('imprint')!=null){ ?>
        <form action="<?php print api_url('upimprint'); ?>" class="form-inline" id="imprint">
        <?php }else{ ?>
            <form action="<?php print api_url('imprint'); ?>" class="form-inline" id="imprint">
            <?php } ?>
            <div class="form-group">
                <label>Imprint (provider identification)</label>
                <textarea class="form-control" id="imprint_editor" name="imprint"><?php if( check('imprint')!=null){  echo check('imprint')->description; }?></textarea>
                <button id="imprintbtn">Change</button>
            </div>
        </form>
        <?php if( check('pp')!=null){ ?>
            <form action="<?php print api_url('uppp'); ?>" class="form-inline" id="pp">
            <?php }else{ ?>
            <form action="<?php print api_url('pp'); ?>" class="form-inline" id="pp">
            <?php } ?>
            <div class="form-group">
                <label>Privacy policy</label>
                <textarea class="form-control" id="privacy_editor"  name="pp"><?php if( check('pp')!=null){  echo check('pp')->description; }?></textarea>
                <button id="ppbtn">Change</button>
            </div>
            </form>
        <?php if( check('agb')!=null){ ?>
            <form action="<?php print api_url('upagb'); ?>" class="form-inline" id="agb">
            <?php }else{ ?>
            <form action="<?php print api_url('agb'); ?>" class="form-inline" id="agb">
            <?php } ?>
            <div class="form-group">
                <label>AGB </label>
                <textarea class="form-control" id="agb_editor"  name="agb"><?php if( check('agb')!=null){  echo check('agb')->description; }?></textarea>
                <button id="agbbtn">Change</button>

            </div>
            </form>
        <?php if( check('cancle')!=null){ ?>
            <form action="<?php print api_url('upcancle'); ?>" class="form-inline" id="cancle">
            <?php }else{ ?>
            <form action="<?php print api_url('cancle'); ?>" class="form-inline" id="cancle">
            <?php } ?>
            <div class="form-group">
                <label>Cancellation policy with cancellation form</label>
                <textarea class="form-control" id="cpolicy_editor"  name="cancle" ><?php if( check('cancle')!=null){  echo check('cancle')->description; }?></textarea>
                <button id="canclebtn">Change</button>

            </div>
            </form>
        <?php if( check('payment')!=null){ ?>
            <form action="<?php print api_url('uppayment'); ?>" class="form-inline" id="payment">
            <?php }else{ ?>
            <form action="<?php print api_url('payment'); ?>" class="form-inline" id="payment">
            <?php } ?>
            <div class="form-group">
                <label>Payment information</label>
                <textarea class="form-control" id="payment_editor"   name="payment"><?php if( check('payment')!=null){  echo check('payment')->description; }?></textarea>
                <button id="paymentbtn">Change</button>

            </div>
            </form>
        <?php if( check('shipping')!=null){ ?>
            <form action="<?php print api_url('upshipping'); ?>" class="form-inline" id="shipping">
            <?php }else{ ?>
            <form action="<?php print api_url('shipping'); ?>" class="form-inline" id="shipping">
            <?php } ?>
            <div class="form-group">
                <label>Shipping information</label>
                <textarea class="form-control" id="shipping_editor"   name="shipping"><?php if( check('shipping')!=null){  echo check('shipping')->description; }?></textarea>
                <button id="shippingbtn">Change</button>

            </div>
            </form>
        <?php if( check('info')!=null){ ?>
            <form action="<?php print api_url('upinfoo'); ?>" class="form-inline" id="info">
            <?php }else{ ?>
            <form action="<?php print api_url('infoo'); ?>" class="form-inline" id="info">
            <?php } ?>
            <div class="form-group">
                <label>Information according to BattG</label>
                <textarea class="form-control" id="battg_editor"  name="infoo" ><?php if( check('infoo')!=null){  echo check('infoo')->description; }?></textarea>
                <button id="infobtn">Change</button>

            </div>
            </form>
        <?php if( check('note')!=null){ ?>
            <form action="<?php print api_url('upnote'); ?>" class="form-inline" id="note">
            <?php }else{ ?>
            <form action="<?php print api_url('note'); ?>" class="form-inline" id="note">
            <?php } ?>
            <div class="form-group">
                <label>Note according to ElektroG</label>
                <textarea class="form-control" id="elektrog_editor"   name="note"><?php if( check('note')!=null){  echo check('note')->description; }?></textarea>
                <button id="notebtn">Change</button>

            </div>

        </form>
    </div>

    <?php
    function check($name){
        $count= DB::table('legals')->where('term_name',$name)->first();
        return $count;
    }
     ?>



    <script src="https://cdn.ckeditor.com/ckeditor5/24.0.0/classic/ckeditor.js"></script>
    <script>

// $("#imprintbtn").on('click',function(){

//         return $.post($('form#imprint').attr('action'), $('form#imprint').serialize(), (res) => {
//             concole.log(res.url);
//         });


// });

// $("#ppbtn").on('click',function(){

//         return $.post($('form#pp').attr('action'), $('form#pp').serialize(), (res) => {
//             $('#input_text').val(res.url)
//         });


// });

// $("#agbbtn").on('click',function(){

//         return $.post($('form#agb').attr('action'), $('form#agb').serialize(), (res) => {
//             $('#input_text').val(res.url)
//         });


// });

// $("#canclebtn").on('click',function(){

//         return $.post($('form#cancle').attr('action'), $('form#cancle').serialize(), (res) => {
//             $('#input_text').val(res.url)
//         });


// });

// $("#paymentbtn").on('click',function(){

//         return $.post($('form#payment').attr('action'), $('form#payment').serialize(), (res) => {
//             $('#input_text').val(res.url)
//         });


// });

// $("#shippingbtn").on('click',function(){

//         return $.post($('form#shipping').attr('action'), $('form#shipping').serialize(), (res) => {
//             $('#input_text').val(res.url)
//         });


// });

// $("#infobtn").on('click',function(){

//         return $.post($('form#info').attr('action'), $('form#info').serialize(), (res) => {
//             $('#input_text').val(res.url)
//         });


// });

// $("#notebtn").on('click',function(){

//         return $.post($('form#note').attr('action'), $('form#note').serialize(), (res) => {
//             $('#input_text').val(res.url)
//         });


// });

        ClassicEditor
            .create(document.querySelector('#imprint_editor'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#privacy_editor'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#agb_editor'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#cpolicy_editor'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#payment_editor'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#shipping_editor'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#battg_editor'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#elektrog_editor'))
            .catch(error => {
                console.error(error);
            });

    </script>
</body>

</html>
