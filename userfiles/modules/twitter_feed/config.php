<?php

$config = array();
$config['name'] = "Twitter feed";

$config['description'] = "Feed of tweets";
$config['author'] = "Peter Ivanov";
$config['ui'] = true;
$config['categories'] = "social";
$config['position'] = 13;
$config['version'] = 0.01;
