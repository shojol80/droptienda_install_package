<?php
/*

  type: layout

  name: Quick Checkout

  description: Quick Checkout

 */
?>

<style>
    .dropdown-menu.shopping-cart {
        min-width: 25rem;
    }

    @media (max-width: 767px){
.cart-table td:nth-of-type(6) {
    text-align: center !important;
}
}
</style>

<?php $total = cart_sum(); ?>

<?php if (is_array($data)) : ?>
    <table class="table table-bordered table-striped mw-cart-table mw-cart-table-medium mw-cart-big-table table-responsive cart-table" >
                <colgroup>
                    <!-- <col width="80"> -->
                    <!-- <col width="200"> -->
                    <!-- <col width="100"> -->
                    <?php if(!isset($_GET['slug'])) { ?>
                    <!-- <col width="140"> -->
                    <?php } ?>
                </colgroup>
                <thead>
                <tr>
                    <th style="width:50px"><?php _e("Bild"); ?></th>
                    <th class="mw-cart-table-product" style="max-width:100px !important"><?php _e("Produktname"); ?></th>
                    <?php if(!isset($_GET['slug'])) { ?>
                        <th style="width:80px"><?php _e("MENGE"); ?></th>
                    <?php } ?>
                    <th><?php _e("Preis"); ?></th>
                    <th><?php _e("Gesamt"); ?></th>
                    <th><?php _e("Löschen"); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $total = cart_sum();
                if(!isset($_GET['slug'])){
//                    dd($data);
                foreach ($data as $item) :

                    //$total += $item['price']* $item['qty'];
                    ?>
                    <tr class="mw-cart-item mw-cart-item-<?php print $item['id'] ?>">
                        <td><?php  if (isset($item['item_image']) and $item['item_image'] != false): ?>
                                <?php $p = $item['item_image']; ?>
                            <?php else:

                                $p = get_picture($item['rel_id']);

                                ?>
                            <?php endif;
//                            @dump($p);?>
                            <?php if ($p != false): ?>
                                <img height="70" class="img-polaroid img-rounded mw-order-item-image mw-order-item-image-<?php print $item['id']; ?>" src="<?php print thumbnail($p, 70, 70); ?>"/>
                            <?php endif; ?></td>
                        <td class="mw-cart-table-product"><?php print $item['title'] ?>
                            <?php if (isset($item['custom_fields'])): ?>
                                <?php print $item['custom_fields'] ?>
                            <?php endif ?></td>
                        <?php if(!isset($_GET['slug'])) { ?>
                            <td><input type="number" min="1" class="input-mini form-control input-sm" value="<?php print $item['qty'] ?>" onchange="mw.cart.qty('<?php print $item['id'] ?>', this.value)"/>
                            </td>
                        <?php } ?>
                        <?php /* <td><?php print currency_format($item['price']); ?></td> */ ?>
                        <td class="mw-cart-table-price"><?php print currency_format($item['price']); ?></td>

                            <td class="mw-cart-table-price"><?php print currency_format($item['price'] * $item['qty']); ?></td>

                        <td style="text-align:center;"><a title="<?php _e("Remove"); ?>" style="color:red;" class="icon-trash" href="javascript:mw.cart.remove('<?php print $item['id'] ?>');"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                    </tr>
                <?php endforeach;
                }else{ foreach ($data as $dataaaa) :?>


                    <tr class="mw-cart-item mw-cart-item-<?php print $dataaaa['id'] ?>">
                        <td><?php  if (isset($dataaaa['item_image']) and $dataaaa['item_image'] != false): ?>
                                <?php $p = $dataaaa['item_image']; ?>
                            <?php else:

                                    $p = get_picture($dataaaa['id']);
                                ?>
                            <?php endif;
                            //                            @dump($p);?>
                            <?php if ($p != false): ?>
                                <img height="70" class="img-polaroid img-rounded mw-order-item-image mw-order-item-image-<?php print $dataaaa['id']; ?>" src="<?php print thumbnail($p, 70, 70); ?>"/>
                            <?php endif; ?></td>
                        <td class="mw-cart-table-product"><?php print $dataaaa['title'] ?>
                            <?php if (isset($dataaaa['custom_fields'])): ?>
                                <?php print $dataaaa['custom_fields'] ?>
                            <?php endif ?></td>



                            <td class="mw-cart-table-price"><?php print currency_format($dataaaa['price']); ?></td>

                        </tr>

                <?php endforeach;
            } ?>

                </tbody>
            </table>
<?php endif; ?>

<?php if (is_ajax()) : ?>

    <script>
        $(document).ready(function () {
            //  cartModalBindButtons();

        });
    </script>

<?php endif; ?>

<div class="products-amount">
    <div class="row">
        <?php if (is_array($data)): ?>
            <div class="col-sm-6 total">
                <p style="margin-bottom:0;text-align:left"><strong><?php _e("Gesamtbetrag: "); ?> <br class="d-none d-sm-block"> <?php print currency_format($total); ?></strong></p>
                <div class="product-tax-text" style="display:flex;align-items:center;font-size: 12px;">
                    <?php $tax= mw()->tax_manager->get();
                    ?>
                    <span class="edit" field="content_body" rel="post">
                        inkl. <?=intval(!empty($tax['0']['amount']) ? $tax['0']['amount'] : 0)?>% MwSt. zzgl.
                    </span>
                    <span>
                        <a href="<?php print site_url("delivery-conditions"); ?>" style="color:#23a1d1;margin-left:5px;">Versand</a>
                    </span>
                </div>
            </div>
            <div class="col-sm-6" style="text-align: right;">
                <a href="<?php echo site_url('checkout') ?>" class="btn btn-primary btn-md btn-block">zur Kasse</a>
            </div>
        <?php else: ?>
            <div class="col-12">
                <p style="margin-left:20px;"><?php _e("Your cart is empty."); ?></p>
            </div>
        <?php endif; ?>
    </div>

</div>
