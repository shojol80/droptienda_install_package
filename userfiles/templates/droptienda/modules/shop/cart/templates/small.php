<?php

/*

type: layout

name: Small

description: Small cart template

*/
  ?>


<script>
    mw.moduleCSS("<?php print $config['url_to_module'] ?>templates/templates.css");

</script>

<div class="cart-small <?php if(is_array($data)==false){print "mw-cart-small-no-items";} ?>  mw-cart-<?php print $params['id']?> <?php print  $template_css_prefix  ?>">
<div>

  <?php if(is_array($data)) :?>

<a href="#" class="cart-small-checkout-link" onclick="carttoggole()">


    <?php
        $total_qty = 0;
        $total_price = 0;
        foreach ($data as $item) {
            $total_qty += $item['qty'];
            $total_price +=  $item['price']* $item['qty'];
        }
      ?>


<span class="mw-cart-small-order-info">
        <i class="glyphicon glyphicon-shopping-cart"></i>
<strong>(<?php print $total_qty; ?>)</strong>
</span>
    <?php
  if(!isset($params['checkout-link-enabled'])){
	  $checkout_link_enanbled =  get_option('data-checkout-link-enabled', $params['id']);
  } else {
	   $checkout_link_enanbled = $params['checkout-link-enabled'];
  }
   ?>
    <?php if($checkout_link_enanbled != 'n') :?>
    <?php $checkout_page =get_option('data-checkout-page', $params['id']); ?>
    <?php if($checkout_page != false and strtolower($checkout_page) != 'default' and intval($checkout_page) > 0){

	   $checkout_page_link = content_link($checkout_page).'/view:checkout';
   } else {
	   $checkout_page_link = checkout_url();

   }

   ?>

    <?php endif ; ?>

    </a>
    <div id="checkout-product" style="display: none;">
        <module type="shop/cart" template="quick_checkout" />
    </div>
</div>
    <?php else : ?>
      <div class="cart-no-item">
        <span><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> <strong>(0)</strong> </span>
      </div>
    <!-- <div class="mw-ui-col">
    <span class="mw-cart-small-order-info"><span class="mw-cart-small-order-info-total no-items"><?php   _e('Your cart is empty') ?></span></span></div> -->
     </div>
    <?php endif ; ?>

      <div style="margin-left:20px;">
    <script>
        var $window = $(window), $document = $(document);
        $document.ready(function () {
            $('.js-register-modal').on('click', function () {
                $(".js-login-window").hide();
                $(".js-forgot-window").hide();
                $(".js-register-window").show();
            });
            $('.js-login-modal').on('click', function () {
                $(".js-register-window").hide();
                $(".js-forgot-window").hide();
                $(".js-login-window").show();
            });
        });
    </script>
    <div class="dropdown btn-member ml-4">
        <a href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php if (user_id()): ?><?php print user_name(); ?><?php else: ?><?php echo _e('Log in'); ?><?php endif; ?></a>
        <ul class="dropdown-menu">
            <?php if (user_id()): ?>
                <li><a href="#" data-toggle="modal" data-target="#loginModal"><?php _e("Profile"); ?></a></li>
                <li><a href="#" data-toggle="modal" data-target="#ordersModal"><?php _e("My Orders"); ?></a></li>

            <?php else: ?>
                <li><a href="#" class="js-login-modal" data-toggle="modal" data-target="#loginModal"><?php _e("Login"); ?></a></li>
                <li><a href="#" class="js-register-modal" data-toggle="modal" data-target="#loginModal"><?php _e("Register"); ?></a></li>
            <?php endif; ?>

            <?php if (is_admin()): ?>
                <li><a href="<?php print admin_url() ?>"><?php _e("Admin panel"); ?></a></li>
            <?php endif; ?>
            <!-- new-code -->

            <?php $userid = Auth::id(); ?>
            <?php if ($userid): ?>
                <li><a data-url="<?php print api_link('delete_user_account') ?>" id="user_delete_btn_fr" href="#"><?php _e("Delete your Account"); ?></a></li>
            <?php endif; ?>

            <?php if (user_id()): ?>

                <li><a href="<?php print api_link('logout') ?>"><?php _e("Logout"); ?></a></li>
            <?php endif; ?>
        </ul>
    </div>


      </div>

</div>
