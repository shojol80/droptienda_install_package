<!DOCTYPE HTML>
<html prefix="og: http://ogp.me/ns#">
<head>
    <title>Droptienda</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="Droptienda"/>
    <meta name="keywords" content="{content_meta_keywords}"/>
    <meta name="description" content="{content_meta_description}"/>
    <meta property="og:type" content="{og_type}"/>
    <meta property="og:url" content="{content_url}"/>
    <meta property="og:image" content="{content_image}"/>
    <meta property="og:description" content="{og_description}"/>
    <meta property="og:site_name" content="{og_site_name}"/>

    <link rel="alternate" type="application/rss+xml" title="{og_site_name}" href="<?php print site_url('rss') ?>"/>


    <link href='//fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Roboto+Slab:400,300&subset=latin,cyrillic,cyrillic-ext,greek,latin-ext' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300italic&subset=latin,cyrillic,greek,latin-ext' rel='stylesheet' type='text/css'>
    <script type="text/javascript">
        mw.lib.require("bootstrap3");
        mw.require(mw.settings.template_url + "js/functions.js");
    </script>

    <link href="https://droptienda.rocks/userfiles/templates/test-temp/catalog/view/javascript/font-awesome/css/font-awesome.min.css">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php print template_url(); ?>css/style.css" type="text/css"/>
    <!-- <link rel="stylesheet" href="<?php print template_url(); ?>css/main.css" type="text/css"/> -->
    <link rel="stylesheet" href="<?php print template_url(); ?>css/main-style.css" type="text/css"/>
    <link rel="stylesheet" href="<?php print template_url(); ?>css/material_icons/material_icons.css" type="text/css"/>
    <link rel="stylesheet" href="<?php print template_url(); ?>css/menu.css" type="text/css"/>
    <link rel="stylesheet" href="<?php print template_url(); ?>css/responsive.css" type="text/css"/>
    <?php $color_scheme2 = get_option('color-scheme2', 'mw-template-liteness'); ?>
    <?php if ($color_scheme2 != '' AND $color_scheme2 != 'lite'): ?>
        <link href="{TEMPLATE_URL}css/color_scheme/<?php print $color_scheme2; ?>.css" rel="stylesheet" type="text/css" id="color_scheme2"/>
    <?php endif; ?>
    <?php include THIS_TEMPLATE_DIR . 'header_options.php'; ?>

    <script>
        mw.require('icon_selector.js');
    </script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
     <script type="text/javascript" src="<?php print template_url(); ?>js/jquery-ui.js"></script>
</head>
<body class="<?php print $font . ' ' . $bgimage; ?> <?php print helper_body_classes(); ?>">
<div id="main-container">
    <div id="header">
        <div class="container edit" field="" rel="global">
            <div class="row">
                <div class="col-md-9">
                    <h1 class="edit element" id="logo" field="logo-top" rel="global">
                        <a href="<?php print site_url(); ?>">
                            <span>Droptienda</span>
                            <small>Define your own Space</small>
                        </a>
                    </h1>
                </div>
                <div class="col-md-3">
                    <div class="header-cart">
                    <div class="">
                                    <a href="#" class="header-cart-link" onclick="carttoggole()">
                                    <span class="mw-cart-small-order-info">
                                            <i class="glyphicon glyphicon-shopping-cart"></i>
                                        <strong>(<strong><?php if(get_cart()){ print count(get_cart()); }else{ print 0; }?></strong>)</strong>
                                    </span>

                                    </a>
                                    <div id="checkout-product" style="display: none;">
                                        <module type="shop/cart" template="quick_checkout" />
                                    </div>
                                </div>
                                <div style="margin-left:20px;">
    <script>
        var $window = $(window), $document = $(document);
        $document.ready(function () {
            $('.js-register-modal').on('click', function () {
                $(".js-login-window").hide();
                $(".js-forgot-window").hide();
                $(".js-register-window").show();
            });
            $('.js-login-modal').on('click', function () {
                $(".js-register-window").hide();
                $(".js-forgot-window").hide();
                $(".js-login-window").show();
            });
        });
    </script>
    <div class="dropdown btn-member ml-4">
        <a href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php if (user_id()): ?><?php print user_name(); ?><?php else: ?><?php echo _e('Log in'); ?><?php endif; ?></a>
        <ul class="dropdown-menu">
            <?php if (user_id()): ?>
                <li><a href="#" data-toggle="modal" data-target="#loginModal"><?php _e("Profile"); ?></a></li>

            <?php else: ?>
                <li><a href="#" class="js-login-modal" data-toggle="modal" data-target="#loginModal"><?php _e("Login"); ?></a></li>
                <li><a href="#" class="js-register-modal" data-toggle="modal" data-target="#loginModal"><?php _e("Register"); ?></a></li>
            <?php endif; ?>

            <?php if (is_admin()): ?>
                <li><a href="<?php print admin_url() ?>"><?php _e("Admin panel"); ?></a></li>
            <?php endif; ?>
            <!-- new-code -->

            <?php $userid = Auth::id(); ?>


            <?php if (user_id()): ?>

                <li><a href="<?php print api_link('logout') ?>"><?php _e("Logout"); ?></a></li>
            <?php endif; ?>
        </ul>
    </div>


      </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="main-menu">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <module type="menu"/>
                    </div>
                    <div class="col-md-3" id="header-search">
                        <module type="search" template="autocomplete"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="content-holder">
    <script type="text/javascript">
    $(".navbar-toggle").on('click', function () {
        $(".navbar-collapse").slideToggle();
    });



    function carttoggole() {
        var x = document.getElementById("checkout-product");
        if (x.style.display === "block") {
            // console.log('block');
            x.style.display = "none";
        } else {
            // console.log('none');

            x.style.display = "block";
        }
    }

</script>


<script>
    $(document).ready(function () {
        $('#loginModal').on('show.bs.modal', function (e) {
            $('#loginModalModuleLogin').reload_module();
            $('#loginModalModuleRegister').reload_module();
        })


        $('#shoppingCartModal').on('show.bs.modal', function (e) {
            $('#js-ajax-cart-checkout-process').reload_module();
        })


        mw.on('mw.cart.add', function (event, data) {
            $('#shoppingCartModal').modal('show');

        })



        <?php if (isset($_GET['mw_payment_success'])) { ?>
        $('#js-ajax-cart-checkout-process').attr('mw_payment_success', true);
        $('#shoppingCartModal').modal('show')

        <?php } ?>


        $('.js-show-register-window').on('click', function () {
            $('.js-login-window').hide();
            $('.js-register-window').show();
        })
        $('.js-show-login-window').on('click', function () {

            $('.js-register-window').hide();
            $('.js-login-window').show();
        })
    })
</script>

<!-- Login Modal -->
<div class="modal login-modal" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body">
                <div class="js-login-window">
                    <div class="icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                    <module type="users/login" id="loginModalModuleLogin" />
                    <!-- <div type="users/login" id="loginModalModuleLogin"></div> -->
                </div>

                <div class="js-register-window" style="display:none;">
                <div class="icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                    <module type="users/register" id="loginModalModuleRegister" />

                    <p class="or"><span>or</span></p>

                    <div class="act login">
                        <a href="#" class="js-show-login-window"><span><i class="fa fa-backward" style="margin-right: 10px;" aria-hidden="true"></i>Back to Login</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $('#cssmenu').prepend('<div id="menu-button">Menu</div>');
        $('#cssmenu #menu-button').on('click', function() {
            var menu = $(".main-nav");
            console.log(menu);
            if (menu.hasClass('open')) {
                menu.removeClass('open');
            } else {
                menu.addClass('open');
            }
        });
    });
</script>