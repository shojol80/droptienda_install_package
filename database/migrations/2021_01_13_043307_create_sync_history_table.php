<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSyncHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sync_history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sync_type', 30);
            $table->string('sync_event', 20);
            $table->bigInteger('model_id');
            $table->timestamp('synced_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->unsignedTinyInteger('tries')->nullable()->default(0);
            $table->bigInteger('drm_ref_id')->nullable();
            $table->text('exception')->nullable();
            $table->text('response')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sync_history');
    }
}
