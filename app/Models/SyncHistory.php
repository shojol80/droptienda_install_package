<?php

namespace App\Models;

use App\Jobs\DrmSyncJob;
use Illuminate\Database\Eloquent\Model;

class SyncHistory extends Model
{
    protected $table = 'sync_history';

    protected $fillable = [
        'sync_type',
        'sync_event',
        'model_id',
        'synced_at',
        'drm_ref_id',
        'tries',
        'exception',
        'response',
    ];


    static function boot ()
    {
    	parent::boot();

    	static::created(function($item) {
        	\Queue::later(1, new DrmSyncJob());
        });
    }
}
