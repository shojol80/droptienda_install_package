<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'cart';

    public function order(){
    	return $this->belongsTo('App\Models\Order', 'order_id');
    }
}
