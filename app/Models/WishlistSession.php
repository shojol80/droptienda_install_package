<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WishlistSession extends Model
{
    public function products()
    {
        return $this->hasManyThrough('App\Models\WishlistSessionProduct', 'App\Models\WishlistSession');
    }
}
